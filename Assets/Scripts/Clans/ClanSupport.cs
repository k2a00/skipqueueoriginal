﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class ClanSupport : MonoBehaviour
{
    [SerializeField] private Text donationAmountUI;
    [SerializeField] private Text availibleCrystalsUI;
    [SerializeField] private CrystalAccountant crystalAccountant;
    [SerializeField] private BelongingToClan belongingToClan;
    [SerializeField] private DonationForm donationForm;
    [SerializeField] private SelectorView selectorView;
    [SerializeField] private Clans clans;
    [SerializeField] private Leaderboard donationLeaderboard;
    [SerializeField] private GameObject leaderboardView;
    [SerializeField] private DonationAccountant donationAccountant;

    public event Action OnNeedSaveData;

    private int donationAmount = 1;
    private int languageIndex = 0;

    private readonly string[] leaderboardsNames = new string[] { "Alfa", "Beta", "Gamma" };

    private readonly string[] JOINTOCLAN = new string[]{ "You should join some clan", "Вам следует вступить в какой-нибудь клан", 
        "Klana katılmanız gerekiyor" };
    private readonly string[] AFTERDONATE = new string[]{ "The crystals have been sent. The rating will be updated tomorrow", 
        "Кристаллы отправлены. Рейтинг обновится завтра", "Kristaller gönderildi. Sıralama yarın güncellenecek" };

    private readonly string[] NOTENOUGH = new string[] { "There are not enough crystals. There are tasks in the main menu. Crystals are a reward",
        "Кристаллов не хватает. В главном меню есть задание, за выполнение которого получите награду",
        "Daha fazla kristale ihtiyacınız var. Ana menüde görevler vardır. Kristaller bir ödüldür" };

    private void Start()
    {
        belongingToClan.OnClanStateChange += RedrawSelector;
        belongingToClan.OnClanStateChange += ChangeUI;

        YandexSDK.Instance.OnRewardADVShowed += OnRewardADVShowed;
    }

    private void OnEnable()
    {
        clans.DownloadClansScore();

        RedrawSelector();

        ChangeUI();

        languageIndex = YandexSDK.Instance.LanguageIndex;

        if (belongingToClan.SelectedClan != Clans.ClansNames.None)
        {
            OrderDonationLeaderboard();
        }
        else 
        {
            leaderboardView.SetActive(false);
        }
    }

    public void ReduceDonation() 
    {
        if (donationAmount > 1)
        {
            donationAmount--;

            ChangeUI();
        }
    }

    public void IncreaseDonation()
    {
        if (crystalAccountant.GetAvailibleCrystal >= donationAmount + 1)
        {
            donationAmount++;

            ChangeUI();
        }
    }

    public void SupportClan()
    {
        if (belongingToClan.SelectedClan == Clans.ClansNames.None)
        {
            MessageBlock.Instance.ShowMessage(JOINTOCLAN[languageIndex]);

            return;
        }

        if (crystalAccountant.SpendCrystals(donationAmount))
        {
            donationForm.SendDonationForm(((int)belongingToClan.SelectedClan).ToString(), donationAmount.ToString());

            donationAccountant.Increase(donationAmount);

            YandexSDK.Instance.SetLeaderBoardScore(leaderboardsNames[(int)belongingToClan.SelectedClan - 1], donationAccountant.Donation);

            donationAmount = 1;

            ChangeUI();

            MessageBlock.Instance.ShowMessage(AFTERDONATE[languageIndex]);

            OnNeedSaveData?.Invoke();
        }
        else
        {
            MessageBlock.Instance.ShowMessage(NOTENOUGH[languageIndex]);
        }
    }

    private void ChangeUI()
    {
        donationAmountUI.text = donationAmount.ToString();
        availibleCrystalsUI.text = crystalAccountant.GetAvailibleCrystal.ToString();
    }

    private void RedrawSelector()
    {
        if (belongingToClan.SelectedClan == Clans.ClansNames.None)
        {
            selectorView.gameObject.SetActive(false);
        }
        else
        {
            selectorView.gameObject.SetActive(true);

            selectorView.ReplaceSelectorCenterMiddle(clans.GetClanTransform(((int)belongingToClan.SelectedClan) - 1));
        }
    }

    private void OnRewardADVShowed(int id)
    {
        ChangeUI();
    }

    private void OrderDonationLeaderboard()
    {
        leaderboardView.SetActive(true);

        donationLeaderboard.OrderLeaderboard(leaderboardsNames[(int)belongingToClan.SelectedClan - 1]);
    }
}
