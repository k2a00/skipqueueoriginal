﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreBoard : MonoBehaviour
{
    public int Points { get; private set; } = 0;
    [SerializeField] private Text scoreOnBoard;

    private Vector3 scoreBoardAsTarget;

    public void ResetScore()
    {
        Points = 0;
        scoreOnBoard.text = Points.ToString();
    }

    public void ShowNewScore(int receivedPoints)
    {
        Points += receivedPoints;

        scoreOnBoard.text = Points.ToString();
    }

    public void RefreshScoreBoard()
    {
        scoreOnBoard.text = Points.ToString();
    }

    public void SetScoreBoardView(Text view)
    {
        scoreOnBoard = view;

        scoreBoardAsTarget = scoreOnBoard.GetComponent<Transform>().position;
    }

    public Vector3 GetScoreBoardViewPosition()
    {
        return scoreBoardAsTarget;
    }
}
