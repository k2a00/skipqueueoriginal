﻿using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Leaderboard : MonoBehaviour
{
    [SerializeField] private LeaderboardElement leaderboardElementPrefab;
    [SerializeField] private Transform parent;

    private List<LeaderboardElement> leaderboardElements = new List<LeaderboardElement>();


    public void OrderLeaderboard(string tableName)
    {
        YandexSDK.Instance.OrderLeaderboardEntries(tableName, CreateLeaderboard);
    }

    public void CreateLeaderboard(string leaderboardRawData)
    {
        for (int i = 0; i < leaderboardElements.Count; i++)
        {
            Destroy(leaderboardElements[i].gameObject);
        }

        leaderboardElements.Clear();

        JSONNode json = JSON.Parse(leaderboardRawData);
        int amount = json["entries"].Count;

        string currentPlayerID = YandexSDK.Instance.PlayerUniqueID;

        for (int i = 0; i < amount; i++)
        {
            string rank = json["entries"][i]["rank"].ToString();
            string name = json["entries"][i]["player"]["publicName"];
            string score = json["entries"][i]["score"].ToString();
            string playerID = json["entries"][i]["player"]["uniqueID"];
 
            LeaderboardElement element = Instantiate(leaderboardElementPrefab, parent);

            if (currentPlayerID == playerID)
            {
                element.ShowPlayerSelector();
            }

            element.SetValues(rank, name, score);

            leaderboardElements.Add(element);
        }
    }
}
