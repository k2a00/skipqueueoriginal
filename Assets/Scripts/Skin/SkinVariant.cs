﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SkinVariant : MonoBehaviour
{
    [SerializeField] private Image image;
    public event Action<int, Transform> OnClickedOnskin;
    private int skinIndex;

    public void SetSkinValues(int index, Sprite sprite)
    {
        skinIndex = index;
        image.sprite = sprite;
    }

    public void SelectSkinVariant()
    {
        OnClickedOnskin.Invoke(skinIndex, transform);
    }
}
