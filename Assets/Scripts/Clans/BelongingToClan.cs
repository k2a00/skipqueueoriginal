﻿using System;
using UnityEngine;

public class BelongingToClan : MonoBehaviour
{
    public Clans.ClansNames SelectedClan { get; private set; } = Clans.ClansNames.None;
    public event Action OnClanStateChange;
    
    [SerializeField] private JoiningClanUI[] joiningClanUI;
    [SerializeField] private Clans clans;
    [SerializeField] private CrystalAccountant crystalAccountant;
    [SerializeField] private DonationForm donationForm;
    [SerializeField] private DonationAccountant donationAccountant;
        
    private bool wasSubscibe;
    private int currentCostForJoin;
    private int currentClanForJoining;

    private readonly int[] costForJoin = new int[] { 5, 3, 1};
    private readonly int costLeaving = 10;

    private readonly string[] NOTENOUGH = new string[] { "There are not enough crystals. There are tasks in the main menu. Crystals are a reward. The cost of joining a clan depends on its place in the ranking",
        "Кристаллов не хватает. В главном меню есть задание, за выполнение которого вы получите награду. Стоимость вступления в клан зависит от его места в рейтинге",
        "Daha fazla kristale ihtiyacınız var. Ana menüde görevler vardır. Kristaller bir ödüldür. Bir klana katılmanın maliyeti, sıralamadaki yerine bağlıdır" };

    private readonly string[] NEEDEDCRYSTALS = new string[] { "Crystals needed:", "Кристаллов нужно:", "Kristallere ihtiyacınız var:" };
    private readonly string[] AGREEMENT = new string[] { "Do you agree to spend them?", "Согласны ли вы их потратить?",
        "Onları harcamayı kabul ediyor musunuz?" };
    
    private readonly string[] AFTERJOIN = new string[] { "Successfully! The rating is updated once a day. Support the clan! Get bonuses!",
        "Успешно! Рейтинг обновляется один раз в день. Поддерживайте клан! Получайте бонусы!", 
        "Bir klana katıldınız! Derecelendirme günde bir kez güncellenir. En iyi bonusları almak için klanı destekleyin!" };
    
    private readonly string[] LEAVECLANNOTICE = new string[] { "Do you want to leave the clan? You will lose clan bonuses",
        "Вы хотите покинуть клан? Вы потеряете клановые бонусы",
        "Klandan ayrılmak mı istiyorsunuz? Klan bonuslarını kaybedeceksiniz" };
    private readonly string[] AFTERLEAVE = new string[] { "You left the clan", "Вы покинули клан", "Klandan ayrıldınız" };


    public void PrepareToJoinClan(Clans.ClansNames clanName)
    {
        int[] rating = clans.ClanRating;
        int clanRatingPlace = 0;

        currentClanForJoining = (int)clanName;
       
        for (int i = 0; i < rating.Length; i++)
        {
            if (rating[i] == currentClanForJoining)
            {
                clanRatingPlace = i;
                break;
            }
        }

        currentCostForJoin = costForJoin[clanRatingPlace];

        int languageIndex = YandexSDK.Instance.LanguageIndex;

        if (crystalAccountant.GetAvailibleCrystal >= costForJoin[clanRatingPlace])
        {
            MessageBlock.Instance.ShowMessage($"{NEEDEDCRYSTALS[languageIndex]} {currentCostForJoin}. {AGREEMENT[languageIndex]}", JoinClan);
        }
        else
        {
            MessageBlock.Instance.ShowMessage($"{NOTENOUGH[languageIndex]}. {NEEDEDCRYSTALS[languageIndex]} {currentCostForJoin}");
        }
    }

    public void PrepareToLeaveClan()
    {
        int languageIndex = YandexSDK.Instance.LanguageIndex;

        if (crystalAccountant.GetAvailibleCrystal >= costLeaving)
        {
            MessageBlock.Instance.ShowMessage($"{LEAVECLANNOTICE[languageIndex]}. {NEEDEDCRYSTALS[languageIndex]} {costLeaving}.", LeaveClan);
        }
        else
        {
            MessageBlock.Instance.ShowMessage($"{NEEDEDCRYSTALS[languageIndex]} {costLeaving}");
        }
    }

    public void LoadData(int clanNumber)
    {
        SelectedClan = (Clans.ClansNames)clanNumber;

        if (SelectedClan != Clans.ClansNames.None)
        {
            if (wasSubscibe)
            {
                UnsubscribeFromJoin();

                return;
            }
        }

        if (wasSubscibe)
        {
            return;
        }

        if (clanNumber == 0)
        {
            SubscribeToJoin();
        }
    }

    private void SubscribeToJoin()
    {
        for (int i = 0; i < joiningClanUI.Length; i++)
        {
            joiningClanUI[i].gameObject.SetActive(true);

            joiningClanUI[i].OnJoinClan += PrepareToJoinClan;
        }

        wasSubscibe = true;
    }

    private void UnsubscribeFromJoin()
    {
        for (int i = 0; i < joiningClanUI.Length; i++)
        {
            joiningClanUI[i].OnJoinClan -= PrepareToJoinClan;

            joiningClanUI[i].gameObject.SetActive(false);
        }

        wasSubscibe = false;
    }

    private void LeaveClan()
    {
        if (crystalAccountant.SpendCrystals(costLeaving))
        {
            donationForm.SendDonationForm(((int)SelectedClan).ToString(), costLeaving.ToString());

            SelectedClan = Clans.ClansNames.None;

            MessageBlock.Instance.ShowMessage(AFTERLEAVE[YandexSDK.Instance.LanguageIndex]);

            donationAccountant.ResetDonations();

            OnClanStateChange?.Invoke();

            SubscribeToJoin();
        }
    }

    private void JoinClan()
    {
        if (crystalAccountant.SpendCrystals(currentCostForJoin))
        {
            SelectedClan = (Clans.ClansNames)currentClanForJoining;

            donationForm.SendDonationForm(((int)SelectedClan).ToString(), currentCostForJoin.ToString());

            MessageBlock.Instance.ShowMessage(AFTERJOIN[YandexSDK.Instance.LanguageIndex]);

            OnClanStateChange?.Invoke();

            UnsubscribeFromJoin();
        }
    }
}
