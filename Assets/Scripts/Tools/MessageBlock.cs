﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MessageBlock : MonoBehaviour
{
    public static MessageBlock Instance { get; private set; }

    [SerializeField] private Text message;
    [SerializeField] private GameObject view;
    [SerializeField] private GameObject okButton;

    private event Action onPressedOK; 
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    public void ShowMessage(string messageText)
    {
        message.text = messageText;

        view.SetActive(true);
        okButton.SetActive(false);
    }

    public void ShowMessage(string messageText, Action _onPressedOK)
    {
        message.text = messageText;

        view.SetActive(true);
        okButton.SetActive(true);

        onPressedOK = _onPressedOK;
    }

    public void OnPressedOK()
    {
        onPressedOK?.Invoke();

        view.SetActive(false);
    }
}
