﻿using UnityEngine;

public class ReceivedPointsCounter : MonoBehaviour
{
    public int ReceivedPointsValue { get; private set; }

    [SerializeField] private int usualPointsValue = 5;
    [SerializeField] private int pointsForFirstGoodDetection = 25;
    [SerializeField] private ClansBonuses clansBonuses;

    private int points = 0;

    private int mistakesMultiplier = 0;
    private int positiveMultiplier = 0;

    private bool needReduceFirstly = true;
    private int bonusForFirstDetection = 0;


    private void Start()
    {
        clansBonuses.OnBonusesChanged += OnBonusesChanged;
    }

    public void ResetCounters()
    {
        positiveMultiplier = 0;
        mistakesMultiplier = 0;
    }

    public void PrepareToStartMatch()
    {
        points = 0;

        ResetCounters();
    }

    public void Increase()
    {
        positiveMultiplier++;

        ReceivedPointsValue = usualPointsValue * positiveMultiplier;

        points += ReceivedPointsValue;
    }

    public void IncreaseFirstly()
    {
        positiveMultiplier++;

        ReceivedPointsValue = pointsForFirstGoodDetection + bonusForFirstDetection;

        points += ReceivedPointsValue;
    }

    public void Reduce()
    {
        mistakesMultiplier++;

        positiveMultiplier = 0;

        ReceivedPointsValue = -usualPointsValue * mistakesMultiplier;

        if (points + ReceivedPointsValue <= 0)
        {
            ReceivedPointsValue = -points;
            
            if (points < 0)
            {
                ReceivedPointsValue = 0;
            }
        }

        points += ReceivedPointsValue;
    }

    public void ReduceFirstly()
    {
        ReceivedPointsValue = 0;

        if (needReduceFirstly)
        {
            positiveMultiplier = 0;

            ReceivedPointsValue = -usualPointsValue;

            points += ReceivedPointsValue;
        }
    }

    private void OnBonusesChanged(bool _needReduceFirstly, int _bonusForFirstDetection)
    {
        needReduceFirstly = _needReduceFirstly;

        bonusForFirstDetection = _bonusForFirstDetection;
    }
}
