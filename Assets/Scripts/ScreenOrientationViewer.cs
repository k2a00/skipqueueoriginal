﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScreenOrientationViewer : MonoBehaviour
{
    public enum ScreenOrientationVariants 
    {
        Portrait, Landscape
    }
    
    public ScreenOrientationVariants CurrentScreenOrientation { get; private set; }

    [SerializeField] private GameObject portraitUI;
    [SerializeField] private GameObject landscapeUI;
    [Space]
    [SerializeField] private Text timerViewPortrait;
    [SerializeField] private Text timerViewLandscape;
    [Space]
    [SerializeField] private Text scoreBoardPortrait;
    [SerializeField] private Text scoreBoardLandscape;
    [Space]
    [SerializeField] private Text countNeededFindViewPortrait;
    [SerializeField] private Text countNeededFindViewLandscape;
    [Space]
    [SerializeField] private GameObject statisticPortait;
    [SerializeField] private GameObject statisticLandscape;
    [Space]
    [SerializeField] private Text changingRoundTimer;
    [Space]
    [SerializeField] private Graph graphPortrait;
    [SerializeField] private Graph graphLandscape;
    [SerializeField] private Text maxPointPortrait;
    [SerializeField] private Text maxPointLandscape;
    
    [Space]
    [SerializeField] private ButtonsLayout buttonsLayout;
    [SerializeField] private GameTimer gameTimer;
    [SerializeField] private ScoreBoard scoreBoard;
    [SerializeField] private RulesWatcher rulesWatcher;
    [SerializeField] private StatisticViewer statisticViewer;



    public void BeginChangingScreenOrientation(string value)
    {
        StartCoroutine(WaitNextFrame(value));
    }

    private void ChangeScreenOrientation(string value)
    {
        if (value == "portrait")
        {
            CurrentScreenOrientation = ScreenOrientationVariants.Portrait;

            scoreBoard.SetScoreBoardView(scoreBoardPortrait);
            rulesWatcher.SetCountNeededFindView(countNeededFindViewPortrait);

            landscapeUI.SetActive(false);
            portraitUI.SetActive(true);

            statisticLandscape.SetActive(false);
            statisticPortait.SetActive(true);

            statisticViewer.SetCurrentValues(graphPortrait, maxPointPortrait);

            buttonsLayout.SetPortraitView();

            gameTimer.SetTimerView(timerViewPortrait);

            changingRoundTimer.fontSize = 170;
        }
        else if (value == "landscape")
        {
            CurrentScreenOrientation = ScreenOrientationVariants.Landscape;

            scoreBoard.SetScoreBoardView(scoreBoardLandscape);
            rulesWatcher.SetCountNeededFindView(countNeededFindViewLandscape);

            portraitUI.SetActive(false);
            landscapeUI.SetActive(true);

            statisticPortait.SetActive(false);
            statisticLandscape.SetActive(true);

            statisticViewer.SetCurrentValues(graphLandscape, maxPointLandscape);

            buttonsLayout.SetLandscapeView();
            
            gameTimer.SetTimerView(timerViewLandscape);

            changingRoundTimer.fontSize = 250;
        }
    }

    private IEnumerator WaitNextFrame(string value)
    {
        yield return new WaitForSeconds(0.3f);

        ChangeScreenOrientation(value);
    }
}
