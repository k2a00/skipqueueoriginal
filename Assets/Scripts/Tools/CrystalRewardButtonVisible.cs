﻿using UnityEngine;

public class CrystalRewardButtonVisible : MonoBehaviour
{
    [SerializeField] private Statistics statistics;
    [SerializeField] private RewardCrystalsButton rewardCrystalsButton;
    [SerializeField] private GameObject view;


    private void OnEnable()
    {
        if (statistics.MatchCount <= 5)
        {
            view.SetActive(false);
        }
        else
        {
            view.SetActive(rewardCrystalsButton.CheckADVAvalible());
        }
    }
}
