﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ChangingRoundTimer : MonoBehaviour
{
    [SerializeField] private Text changingRoundTimerView;
    
    private Action timerCallback;


    public void PrepareToStart(float timerValue, Action callback)
    {
        timerCallback = callback;
        StartCoroutine(StartTimer(timerValue));
    }

    private IEnumerator StartTimer(float timerValue)
    {
        float timer = timerValue;
        int integerTimer = Mathf.RoundToInt(timer);

        changingRoundTimerView.text = integerTimer.ToString();

        while (timer > 0)
        {
            timer -= 1 * Time.deltaTime;

            if (integerTimer - timer >= 1)
            {
                integerTimer = Mathf.RoundToInt(timer);

                changingRoundTimerView.text = integerTimer.ToString();
            }

            yield return null;
        }

        changingRoundTimerView.text = "GO!";

        yield return new WaitForSeconds(1f);

        changingRoundTimerView.text = "";

        timerCallback.Invoke();
    }
}
