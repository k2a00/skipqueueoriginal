﻿using System.Collections;
using UnityEngine;

public class ReceivedPointsSpawner : MonoBehaviour
{
    [SerializeField] private ReceivedPoints[] receivedPoints;

    [SerializeField] private ButtonsStorage buttonsStorage;
    [SerializeField] private ReceivedPointsCounter pointCounter;
    [SerializeField] private ScoreBoard scoreBoard;

    private int receivedPointsLength;



    private void OnEnable()
    {
        for (int i = 0; i < buttonsStorage.BinaryButtons.Count; i++)
        {
            buttonsStorage.BinaryButtons[i].OnPressButton += OnPressButton;
        }
    }

    private void Start()
    {
        receivedPointsLength = receivedPoints.Length;
    }

    private void OnPressButton(BinaryButton binaryButton)
    {
        StartCoroutine(SpawnReceivedPoints(binaryButton));
    }

    private IEnumerator SpawnReceivedPoints (BinaryButton binaryButton)
    {
        yield return new WaitForEndOfFrame();

        for (int i = 0; i < receivedPointsLength; i++)
        {
            if (receivedPoints[i].gameObject.activeSelf == false)
            {
                receivedPoints[i].Init(binaryButton.transform.position, scoreBoard.GetScoreBoardViewPosition(), pointCounter.ReceivedPointsValue);
                
                receivedPoints[i].gameObject.SetActive(true);

                break;
            }
        }
    }
}
