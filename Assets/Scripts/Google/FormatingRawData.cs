﻿using UnityEngine;

public class FormatingRawData : MonoBehaviour
{
    private const char cellSeporator = ',';


    public int[] FormatData(string cvsRawData)
    {
        char lineEnding = GetPlatformSpecificLineEnd();
        
        string[] lines = cvsRawData.Split(lineEnding);
        
        string[] cells = lines[0].Split(cellSeporator);

        int[] clansScore = new int[3];
        
        for (int i = 0; i < 3; i++)
        {
            clansScore[i] = ParseInt(cells[i]);
        }

        return clansScore;
    }

    private int ParseInt(string s)
    {
        int result = -1;
        if (!int.TryParse(s, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.GetCultureInfo("en-US"), out result))
        {
            Debug.Log("Can't parse int, wrong text");
        }

        return result;
    }

    private char GetPlatformSpecificLineEnd()
    {
        char lineEnding = '\n';
#if UNITY_IOS
        lineEnding = '\r';
#endif
        return lineEnding;
    }
}