﻿using System;
using UnityEngine;

public class Clans : MonoBehaviour
{
    public enum ClansNames { None, Alpha, Beta, Gamma }

    [SerializeField] private ClanInformation[] clansInformations;
    [SerializeField] private CVSLoader cvsLoader;
    [SerializeField] private FormatingRawData formatingRawData;
    [SerializeField] private BelongingToClan belongingToClan;

    public event Action OnClansRatingChanged;

    public int DayRatingUpdate { get; private set; }
    public int[] ClansScore { get; private set; } = new int[3];
    public int[] ClanRating { get; private set; } = new int[3];

    private int currentDay;
    private bool ratingWasLoad;


    public void SetCurrentDay(int day)
    {
        currentDay = day;
    }

    public void DownloadClansScore()
    {
        if (currentDay == DayRatingUpdate)
        {
            return;
        }

        if (ratingWasLoad)
        {
            return;
        }

        ratingWasLoad = true;

        cvsLoader.DownloadTable(OnRawCVSLoaded);
    }

    public Transform GetClanTransform(int clanNumber)
    {
        return clansInformations[clanNumber].transform;
    }

    public void LoadData(int updatingDay, int[] score)
    {
        if (!ratingWasLoad)
        {
            DayRatingUpdate = updatingDay;

            if (currentDay == DayRatingUpdate)
            {
                ClansScore = score;

                RebuildRating(score);

                return;
            }
            
            if (belongingToClan.SelectedClan != ClansNames.None)
            {
                DownloadClansScore();
            }
        }
    }

    private void OnRawCVSLoaded(string rawCVSText)
    {
        int[] scores = formatingRawData.FormatData(rawCVSText);

        ClansScore = scores;
        DayRatingUpdate = currentDay;

        RebuildRating(scores);
    }

    private void RebuildRating(int[] scores)
    {
        int clansCount = clansInformations.Length;
        int[] sortingScore = new int[3];

        for (int i = 0; i < clansCount; i++)
        {
            sortingScore[i] = scores[i];
        }

        for (int i = 0; i < clansCount; i++)
        {
            clansInformations[i].SetRatingScore(scores[i]);

            ClanRating[i] = i + 1;

            for (int a = 0; a < clansCount; a++)
            {
                if (sortingScore[i] < sortingScore[a])
                {
                    if (i < a)
                    {
                        int temporary = sortingScore[i];   
                        sortingScore[i] = sortingScore[a];
                        sortingScore[a] = temporary;
                    }
                }
            }
        }

        for (int i = 0; i < clansCount; i++)
        {
            for (int a = 0; a < clansCount; a++)
            {
                if (scores[i] == sortingScore[a])
                {
                    clansInformations[i].transform.SetSiblingIndex(a);

                    sortingScore[a] = -1;

                    ClanRating[a] = i + 1;

                    break;
                }
            }
        }

        OnClansRatingChanged?.Invoke();
    }
}