﻿using UnityEngine;

public class SelectorView : MonoBehaviour
{
    [SerializeField] private RectTransform thisTransform;


    public void ReplaceSelectorCenterMiddle(Transform parent)
    {
        thisTransform.parent = parent;

        thisTransform.anchorMin = new Vector2(0.5f, 0.5f);
        thisTransform.anchorMax = new Vector2(0.5f, 0.5f);
        thisTransform.localPosition = Vector3.zero;
    }
}
