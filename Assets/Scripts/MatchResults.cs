﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MatchResults : MonoBehaviour
{
    [SerializeField] private GameObject resultsPanel;
    [SerializeField] private Text scoreBoard;

    public void ShowResults(int points)
    {
        resultsPanel.SetActive(true);
        scoreBoard.text = points.ToString();
    }
}