﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class YandexSDK : MonoBehaviour
{
    [SerializeField] private ScreenOrientationViewer screenOrientationViewer;
    [SerializeField] private RewardCrystalsButton rewardCrystalsButton;
    [SerializeField] private Clans clans;
    [SerializeField] private MusicMuteStatus musicMuteStatus;

    private static YandexSDK _instance;
    public static YandexSDK Instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<YandexSDK>();

            return _instance;
        }
    }
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    public UserGameData GetUserGameData { get; private set; } = new UserGameData();
    public int LanguageIndex { get; private set; } = 0;
    public string PlayerUniqueID { get; private set; } = "unlogin";
    public bool AuthSuccess { get; private set; }

    public event Action OnAuthenticated;
    public event Action OnAuthInGame;
    public event Action OnDataFromAccountGot;
    public event Action OnLocalDataGot;    
    public event Action<int> OnRewardADVShowed;
    public event Action OnLanguageSet;

    [DllImport("__Internal")]
    private static extern void Auth();    
    [DllImport("__Internal")]
    private static extern void AuthInGame();
    [DllImport("__Internal")]
    private static extern void ShowCommonADV(); 
    [DllImport("__Internal")]
    private static extern void SaveDataToAllStorage(string data);
    [DllImport("__Internal")]
    private static extern void SaveDataToLocalStorage(string data);    
    [DllImport("__Internal")]
    private static extern void SetLeaderboardScore(string tableName, int score);
    [DllImport("__Internal")]
    private static extern void ShowRewardADV(int id);        
    [DllImport("__Internal")]
    private static extern void GetLeaderBoardEntries(string tableName);    

    private bool isAuthInGame;
    private bool isNeedToUnmuteAudio;

    private readonly float adTimerValue = 200f;
    private float adTimer = 200f;

    private Queue<LeaderboardConfig> leaderboardConfigs = new Queue<LeaderboardConfig>(); 
    private Action<string> onLeaderboardGot;


    private void Start()
    {
        StartCoroutine(WaitNextFrame(Authenticate));  
    }

    private void Update()
    {
        if (adTimer > 0)
        {
            adTimer -= 1 * Time.deltaTime;
        }
    }

    public void SetCorrectLanguage(string lang)
    {
        if (lang == "en")
        {
            LanguageIndex = 0;
        }
        else if (lang == "ru")
        {
            LanguageIndex = 1;
        }
        else if (lang == "tr")
        {
            LanguageIndex = 2;
        }

        OnLanguageSet?.Invoke();
    }

    public void Authenticate()  
    {
         Auth();
    }

    public void AuthenticateInGame()
    {
        isAuthInGame = true;

        AuthInGame();
    }

    public void SetDay(int day)
    {
        clans.SetCurrentDay(day);
    }

    public void SetHour(int hour)
    {
        rewardCrystalsButton.SetHour(hour);
    }

    public void SetPlayerID(string id)
    {
        PlayerUniqueID = id;
    }

    public void SaveDataToStorages(string data)
    {
        if (!AuthSuccess)
        {
            SaveDataToLocalStorage(data);

            return;
        }

        SaveDataToAllStorage(data);
    }

    public void SetLeaderBoardScore(string tableName, int score)
    {
        if (!AuthSuccess)
        {
            return;
        }
            
        LeaderboardConfig config = new LeaderboardConfig()
        {
            TableName = tableName,
            Score = score
        };

        leaderboardConfigs.Enqueue(config);

        if (leaderboardConfigs.Count == 1)
        {
            SetValueToLeaderboard();
        }
    }

    public void ShowCommonAdvertisment()    
    {
        if (adTimer <= 0)
        {
            adTimer = adTimerValue;

            MuteMusic();

            ShowCommonADV();
        }
    }

    public void ShowRewardAdvertisment(int id)    
    {
        MuteMusic();

        ShowRewardADV(id);
    }

    public void AuthenticateSuccess()
    {
        AuthSuccess = true;
        
        OnAuthenticated?.Invoke();
    }

    public void SetLocalData(string data)
    {
        UserDataSaving UDS = new UserDataSaving();
        UDS = JsonUtility.FromJson<UserDataSaving>(data);
        GetUserGameData = JsonUtility.FromJson<UserGameData>(UDS.data);

        StartCoroutine(WaitNextFrame(OnLocalDataGot));
    }

    public void SetDataFromAccount(string data)
    {
        UserDataSaving UDS = new UserDataSaving();
        UDS = JsonUtility.FromJson<UserDataSaving>(data);
        GetUserGameData = JsonUtility.FromJson<UserGameData>(UDS.data);

        StartCoroutine(WaitData());
    }

    public void OrderLeaderboardEntries(string tableName, Action<string> callback)
    {
        onLeaderboardGot = callback;

        GetLeaderBoardEntries(tableName);
    }

    public void SetLeaderboardRawData(string leaderboardRawData)
    {
        onLeaderboardGot?.Invoke(leaderboardRawData);
    }

    public void OnLeaderboardScoreSet()
    {
        leaderboardConfigs.Dequeue();

        if (leaderboardConfigs.Count != 0)
        {
            SetValueToLeaderboard();
        }
    }

    public void RewardADVShowed(int id)
    {
        OnRewardADVShowed?.Invoke(id);

        UnmuteMusic();
    } 

    public void UnmuteMusic()
    {
        if (isNeedToUnmuteAudio)
        {
            musicMuteStatus.ChangeMuteState();
        }
    }

    public void SetScreenOrientation(string value)
    {
        screenOrientationViewer.BeginChangingScreenOrientation(value);
    }

    private void MuteMusic()
    {
        isNeedToUnmuteAudio = false;

        if (!musicMuteStatus.GetCurrentMuteState())
        {
            musicMuteStatus.ChangeMuteState();
            isNeedToUnmuteAudio = true;
        }
    }

    private void SetValueToLeaderboard()
    {
        LeaderboardConfig config = leaderboardConfigs.Peek();

        SetLeaderboardScore(config.TableName, config.Score);
    }

    private IEnumerator WaitNextFrame(Action methodAfterWaiting)
    {
        yield return null;

        methodAfterWaiting?.Invoke();
    }

    private IEnumerator WaitData()
    {
        yield return new WaitForSeconds(0.5f);

        OnDataFromAccountGot?.Invoke();

        if (isAuthInGame)
        {
            OnAuthInGame?.Invoke();
        }
    }
}


[System.Serializable]
public class UserGameData
{
    public int CrystalReceived;
    public int CrystalSpent;
    public int RewardCrystalHour;
    public int ADVShowed;

    public int ClanNumber;
    public int RatingLoadingDay;
    public int Clan1Score;
    public int Clan2Score;
    public int Clan3Score;
    public int Donation;

    public int MatchesNeed;
    public int TotalPointsNeed;
    public int PointsPerMatch;
    public int MatchesPlayed;
    public int PointsReceived;

    public int MaxPoint;
    public int MatchCount;
    public int result1;
    public int result2;
    public int result3;
    public int result4;
    public int result5;
    public int result6;
    public int result7;
    public int result8;
    public int result9;
    public int result10;
    public int result11;
    public int result12;
    public int result13;
    public int result14;
    public int result15;
    public int result16;
    public int result17;
    public int result18;
    public int result19;
    public int result20;
    public int result21;
    public int result22;
    public int result23;
    public int result24;
    public int result25;
    public int result26;
    public int result27;
    public int result28;
    public int result29;
    public int result30;
}

[Serializable]
public class UserDataSaving
{
    public string data;
}