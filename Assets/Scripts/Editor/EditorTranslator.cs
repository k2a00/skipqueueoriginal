﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Translator))]
public class EditorTranslator : Editor
{
    private Translator translator;


    private void Awake()
    {
        translator = (Translator)target;
    }


    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Previous"))
        {
            translator.GetPrevious();
        }

        if (GUILayout.Button("Add empty"))
        {
            translator.AddEmpty();
        }

        if (GUILayout.Button("Next"))
        {
            translator.GetNext();
        }

        GUILayout.EndHorizontal();

        if (GUILayout.Button("Confirm"))
        {
            translator.AddObject();
        }

        if (GUILayout.Button("Remove"))
        {
            translator.RemoveCurrentObject();
        }
    }
}
