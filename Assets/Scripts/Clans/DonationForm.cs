﻿using UnityEngine;

public class DonationForm : MonoBehaviour
{
    [SerializeField] private GoogleFormSender googleFormSender;

    private readonly string formUrl = "https://docs.google.com/forms/u/0/d/e/1FAIpQLSe3Nlo7Nu38uaf_kgKjTs_DQ7gMdqTbPslAzGRq4oKyB1LMPQ/formResponse";
    private readonly string clanNumberEntry = "entry.200993715";
    private readonly string donationAmountEntry = "entry.1136830436";


    public void SendDonationForm(string clanNumber, string donationAmount)
    {
        ConfigForFormSending[] config = new ConfigForFormSending[2] { new ConfigForFormSending(), new ConfigForFormSending() };

        config[0].EntryID = clanNumberEntry;
        config[0].Value = clanNumber;

        config[1].EntryID = donationAmountEntry;
        config[1].Value = donationAmount;

        googleFormSender.SendData(formUrl, config);
    }
}
