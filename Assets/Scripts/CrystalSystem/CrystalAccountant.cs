﻿using UnityEngine;

public class CrystalAccountant : MonoBehaviour
{
    public int CrystalReceived { get; private set; } = 1;
    public int CrystalSpent { get; private set; } = 0;


    public int GetAvailibleCrystal => CrystalReceived - CrystalSpent;

    public void Increase()
    {
        CrystalReceived++;
    }

    public bool SpendCrystals(int value)
    {
        if (GetAvailibleCrystal - value < 0)
        {
            return false;
        }

        CrystalSpent += value;

        return true;
    }

    public void LoadCrystalCount(int received, int spent)
    {
        CrystalReceived = received;
        CrystalSpent = spent;
    }
}
