﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StatisticViewer : MonoBehaviour
{
    [SerializeField] private Text maxPoints;
    [SerializeField] private GameObject statisticsPanel;
    [Space]
    [SerializeField] private Statistics statistics;
    [SerializeField] private ScreenOrientationViewer screenOrientationViewer;

    private Graph graph;
    private readonly int valuesInterval = 10;
    private readonly float timeForLoadData = 1f;


    private void Start()
    {
        statistics.OnDataChecked += CheckStatisticShowingNecessary;   
    }

    public void ShowStatistic()
    {
        statisticsPanel.SetActive(true);

        BuildGraph();
    }

    public void SetCurrentValues(Graph _graph, Text _maxPoints)
    {
        graph = _graph;
        maxPoints = _maxPoints;

        if (statisticsPanel.activeSelf)
        {
            BuildGraph();
        }
    }

    private void CheckStatisticShowingNecessary()
    {
        if (statisticsPanel.activeSelf)
        {
            StartCoroutine(WaitDataLoading());
        }
    }

    private void BuildGraph()
    {
        float[] pointsForMatches = statistics.GetPointsForMatches();

        if (pointsForMatches.Length == 0)
        {
            StartCoroutine(WaitDataLoading());
        }

        maxPoints.text = statistics.MaxPoints.ToString();
        
        if (screenOrientationViewer.CurrentScreenOrientation == ScreenOrientationViewer.ScreenOrientationVariants.Portrait)
        {
            graph.AddValuesForShiftingGraph(pointsForMatches, valuesInterval);
            graph.DrawGraph();

            return;
        }

        graph.AddValues(pointsForMatches);
        graph.DrawGraph();
    }

    private IEnumerator WaitDataLoading()
    {
        yield return new WaitForSeconds(timeForLoadData);

        BuildGraph();
    }
}
