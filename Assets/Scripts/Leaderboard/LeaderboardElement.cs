﻿using UnityEngine;
using UnityEngine.UI;

public class LeaderboardElement : MonoBehaviour
{
    [SerializeField] private Text rank;
    [SerializeField] private Text userName;
    [SerializeField] private Text score;
    [SerializeField] private Image icon;
    [SerializeField] private GameObject playerSelector;


    private void Start()
    {
        icon.color = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f), 1);
    }

    public void SetValues(string _rank, string _userName, string _score)
    {
        rank.text = _rank;

        if (_userName != "")
        {
            userName.text = _userName;
        }

        score.text = _score;
    }

    public void ShowPlayerSelector()
    {
        playerSelector.SetActive(true);
    }
}
