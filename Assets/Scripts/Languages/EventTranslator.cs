﻿using UnityEngine;

public class EventTranslator : MonoBehaviour
{
    [SerializeField] private EventVocabulary eventVocabularies;


    public void SetCorrectLanguage(int resultEvent)
    {
        eventVocabularies.TextPlace.text = eventVocabularies.Equivalents[resultEvent].Equivalents[YandexSDK.Instance.LanguageIndex];
    }
}