﻿using UnityEngine;
using UnityEngine.UI;

public class ReceivedPoints : MonoBehaviour
{
    public int ReceivedPointsValue { get; private set; }
    [SerializeField] private float speed;
    [SerializeField] private Transform thisTransform;
    [SerializeField] private Text pointsValueView;
    [SerializeField] private Color goodResult;
    [SerializeField] private Color badResult;

    private Vector3 target;
    private float enableTimerValue = 6f;
    private float enableTimer = 6f;


    private void Update()
    {
        enableTimer -= 1 * Time.deltaTime;

        if (enableTimer < 0)
        {
            gameObject.SetActive(false);
        }
        else
        {
            MoveToTarget();
        }
    }

    public void Init(Vector3 startPosition , Vector3 _target, int receivedPoints)
    {
        target = _target;
        ReceivedPointsValue = receivedPoints;
        thisTransform.position = startPosition;

        enableTimer = enableTimerValue;

        if (receivedPoints > 0)
        {
            pointsValueView.color = goodResult;
            pointsValueView.text = "+" + receivedPoints.ToString();
        }
        else
        {
            pointsValueView.color = badResult;
            pointsValueView.text = receivedPoints.ToString();
        }
    }

    private void MoveToTarget()
    {
        thisTransform.position = Vector3.MoveTowards(thisTransform.position, target, speed * Time.deltaTime); 
    }
}
