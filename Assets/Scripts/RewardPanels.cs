﻿using System.Collections;
using UnityEngine;

public class RewardPanels : MonoBehaviour
{
    [SerializeField] private GameObject[] panels;


    public void HideRewardPanels()
    {
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }
    }

    public void ShowRewardPanels()
    {
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(true);
        }
    }
}
