﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RulesWatcher : MonoBehaviour
{
    [SerializeField] private float timeToChangingRound = 3f;
    [SerializeField] private float matchTimerValue = 60f;

    [SerializeField] private ButtonsStorage buttonsStorage;
    [SerializeField] private ReceivedPointsCounter pointCounter;
    [SerializeField] private GameTimer matchTimer;
    [SerializeField] private GameTimer changingRoundTimer;
    [SerializeField] private ScoreBoard scoreBoard;
    [SerializeField] private MatchResults matchResults;
    [SerializeField] private Statistics statistics;
    [SerializeField] private RewardPanels rewardPanels;
    [SerializeField] private Leaderboard afterMatchLeaderboard;

    private Text countNeededFind;
    
    private int maxGoodButtons;
    private int pressedGoodButtons = 0;
    private readonly float waitingTimeForFinalScore = 3f;

    private readonly string[] GO = new string[3] { "GO!", "ВПЕРЁД!", "GO!" };
    private string go;
    private readonly string[] SKIPQUEUE = new string[3] { "#SKIPQUEUE", "#СКИПКВИ", "#SKIPQUEUE" };
    private string skipQueue;

    private readonly int idRewardADV = 1;



    private void OnEnable()
    {
        YandexSDK.Instance.OnLanguageSet += SetCorrectLanguage;
        YandexSDK.Instance.OnRewardADVShowed += OnRewardADVShowed; ;

        for (int i = 0; i < buttonsStorage.BinaryButtons.Count; i++)
        {
            buttonsStorage.BinaryButtons[i].OnPressButton += OnPressButton;
        }
    }

    public void SetMaxGoodButtonsCount(int count)
    {
        maxGoodButtons = count;
    }

    public void OnPressButton(BinaryButton binaryButton)
    {
        if (binaryButton.GetBinaryButtonState() == BinaryButtonState.State.GoodButton)
        {
            pressedGoodButtons++;
            
            if (binaryButton.WasPressInRound)
            {
                pointCounter.Increase();
            }
            else
            {
                pointCounter.IncreaseFirstly();
            }

            if (maxGoodButtons == pressedGoodButtons)
            {
                PrepareToChangingRound();
            }
        }
        else
        {
            if (binaryButton.WasPressInRound)
            {
                pointCounter.Reduce();

            }
            else
            {
                pointCounter.ReduceFirstly();
            }

            pressedGoodButtons = 0;

            buttonsStorage.SetDefaultValues();
        }

        countNeededFind.text = (maxGoodButtons - pressedGoodButtons).ToString();
    }

    public void PrepareToStartMatch()
    {
        buttonsStorage.SetUninteractableMode();

        scoreBoard.ResetScore();
        pointCounter.PrepareToStartMatch();

        HideRewardPanel();

        changingRoundTimer.StartTimer(timeToChangingRound, StartMatch, go, 2f);
    }

    public void LeaveMatch()
    {
        matchTimer.ResetTimer();
        changingRoundTimer.ResetTimer();

        buttonsStorage.SetDefaultValues();
    }

    public void RestartMatch()
    {
        matchTimer.ResetTimer();
        pointCounter.PrepareToStartMatch();
        buttonsStorage.SetDefaultValues();
        
        PrepareToStartMatch();
    }

    public void WaitFinalScore()
    {
        buttonsStorage.SetUninteractableMode();
     
        changingRoundTimer.StartTimer(waitingTimeForFinalScore, FinishMatch, skipQueue, 1f);
    }

    public void SetCountNeededFindView(Text value)
    {
        countNeededFind = value;

        countNeededFind.text = (maxGoodButtons - pressedGoodButtons).ToString();
    }

    public void PrepareToChangingRound()
    {
        pressedGoodButtons = 0;

        matchTimer.Pause();

        buttonsStorage.SetUninteractableMode();

        changingRoundTimer.StartTimer(timeToChangingRound, ChangeRound, go, 1.5f);
    }

    private void StartMatch()
    {
        buttonsStorage.PrepareToRound();

        countNeededFind.text = maxGoodButtons.ToString();

        scoreBoard.ResetScore();
        pressedGoodButtons = 0;

        matchTimer.StartTimer(matchTimerValue, WaitFinalScore);

        ShowRewardPanels();
    }

    private void ChangeRound()
    {
        pointCounter.ResetCounters();
        buttonsStorage.PrepareToRound();

        countNeededFind.text = maxGoodButtons.ToString();

        matchTimer.Unpause();
    }

    private void FinishMatch()
    {
        statistics.AddMatchStatistics();

        matchResults.ShowResults(scoreBoard.Points);

        StartCoroutine(OrderLeaderboard());

        YandexSDK.Instance.ShowCommonAdvertisment();
    }

    private IEnumerator OrderLeaderboard()
    {
        yield return new WaitForSeconds(1);

        afterMatchLeaderboard.OrderLeaderboard("AfterMatchScore");
    }

    private void HideRewardPanel()
    {
        rewardPanels.HideRewardPanels();
    }

    private void ShowRewardPanels()
    {
        rewardPanels.ShowRewardPanels();
    }

    private void OnRewardADVShowed(int id)
    {
        if (id == idRewardADV)
        {
            PrepareToChangingRound();
        }
    }

    private void SetCorrectLanguage()
    {
        int langugeIndex = YandexSDK.Instance.LanguageIndex;

        go = GO[langugeIndex];
        skipQueue = SKIPQUEUE[langugeIndex];
    }
}
