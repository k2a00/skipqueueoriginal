﻿using System.Collections.Generic;
using UnityEngine;

public class SkinStorage : MonoBehaviour
{
    [SerializeField] private Sprite[] skins;
    [SerializeField] private SkinVariant skinVariant;
    [SerializeField] private Transform parent;
    [Space]
    [SerializeField] private SkinSelector skinSelector;

    private List<SkinVariant> instantiatedSkinVariants = new List<SkinVariant>();

    public void InstantiateSkinVariants()
    {
        if (instantiatedSkinVariants.Count != 0)
        {
            return;
        }

        for (int i = 0; i < skins.Length; i++)
        {
            SkinVariant variant = Instantiate(skinVariant, parent);

            instantiatedSkinVariants.Add(variant);

            variant.SetSkinValues(i, skins[i]);
            skinSelector.BecomeListener(variant);

            if (skinSelector.SelectedSkin == i)
            {
                skinSelector.ReplaceSelector(variant.transform);
            }
        }
    }

    public Sprite GetCurrentSkin(int index)
    {
        return skins[index];
    }
}
