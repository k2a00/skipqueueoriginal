﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class InteractableDelay : MonoBehaviour
{
    [SerializeField] private Button thisButton;

    private void OnEnable()
    {
        StartCoroutine(StartInteractableDelay());
    }

    private IEnumerator StartInteractableDelay()
    {
        thisButton.interactable = false;

        yield return new WaitForSeconds(3);

        thisButton.interactable = true;
    }
}
