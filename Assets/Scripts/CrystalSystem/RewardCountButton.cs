﻿using UnityEngine;

public class RewardCountButton : MonoBehaviour
{
    [SerializeField] private Statistics statistics;
    [SerializeField] private GameObject view;


    private void OnEnable()
    {
        if (statistics.MatchCount <= 5)
        {
            view.SetActive(false);
        }
        else
        {
            view.SetActive(true);
        }
    }
}
