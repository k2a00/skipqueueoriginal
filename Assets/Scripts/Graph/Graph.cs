﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Graph : MonoBehaviour
{
    [Header("Prefabs")]
    [SerializeField] private RectTransform pointPrefab;
    [SerializeField] private RectTransform xAxisMarkPrefab;
    [SerializeField] private RectTransform yAxisMarkPrefab;

    [Space]
    public bool isXMarkSignatureAdjust = true;

    [Space]
    [SerializeField] private string xValueAdditionalSignature;
    [SerializeField] private string yValueAdditionalSignature;
    
    [Space]
    [SerializeField] private RectTransform axisZone;
    [SerializeField] private RectTransform xAxis;
    [SerializeField] private RectTransform yAxis;
    [SerializeField] private LineRenderer lineRenderer;

    [Space]
    [SerializeField] private float borderOffset;

    private List<RectTransform> temporaryObjects;

    private Vector2[] allValuesForShiftingGraph;
    private Vector2[] currentValues;
    private Vector3[] lineRendererPositions;

    private int intervalInShiftingGraph;
    private int currentPartOfShiftingGraph = 0;
    private float xMarkOffset;
    private float yMarkOffset;
    private readonly float minPartOfAxisForMarkOffset = 0.05f;


    private void Awake()
    {
        temporaryObjects = new List<RectTransform>();
    }


    public void AddValues(float[] _values)
    {
        ResetGraph();

        int valuesLengt = _values.Length;

        currentValues = new Vector2[valuesLengt];

        for (int i = 0; i < valuesLengt; i++)
        {
            float x = i+1;

            currentValues[i] = new Vector2(x , _values[i]);
        }

        AdjustCoordinates();
    }

    public void AddValuesForShiftingGraph(float[] _values, int _interval)
    {
        currentPartOfShiftingGraph = 0;

        intervalInShiftingGraph = _interval;

        int valuesLengt = _values.Length;

        allValuesForShiftingGraph = new Vector2[valuesLengt];

        for (int i = 0; i < valuesLengt; i++)
        {
            float x = i + 1;

            allValuesForShiftingGraph[i] = new Vector2(x, _values[i]);
        }

        if (valuesLengt <= intervalInShiftingGraph)
        {
            AddValues(allValuesForShiftingGraph);

            return;
        }

        float[] intervalValues = new float[intervalInShiftingGraph];

        for (int i = 0; i < intervalInShiftingGraph; i++)
        {
            intervalValues[i] = allValuesForShiftingGraph[i].y;
        }

        AddValues(intervalValues);
    }

    public void AddValues(Vector2[] _values)
    {
        ResetGraph();

        int length = _values.Length;

        currentValues = new Vector2[length];
        
        currentValues = _values;

        AdjustCoordinates();
    }

    public void DrawGraph()
    {
        lineRenderer.positionCount = lineRendererPositions.Length;
        lineRenderer.SetPositions(lineRendererPositions);

        for (int i = 0; i < lineRendererPositions.Length; i++)
        {
            RectTransform go = Instantiate(pointPrefab, axisZone);
            go.localPosition = lineRendererPositions[i];

            temporaryObjects.Add(go);
        }
    }

    public void ShiftRight()
    {
        int pointsFullIntervalLength = allValuesForShiftingGraph.Length;

        if (pointsFullIntervalLength <= intervalInShiftingGraph * (currentPartOfShiftingGraph + 1))
        {
            return;
        }
        
        currentPartOfShiftingGraph++;

        int length = intervalInShiftingGraph;

        if (pointsFullIntervalLength - intervalInShiftingGraph * (currentPartOfShiftingGraph + 1) < 0)
        {
            length = pointsFullIntervalLength - intervalInShiftingGraph * currentPartOfShiftingGraph;
        }

        Vector2[] intervalValues = new Vector2[length];
        
        int intervalRange = intervalInShiftingGraph * currentPartOfShiftingGraph;
        
        for (int i = 0; i < length; i++)
        {
            intervalValues[i] = allValuesForShiftingGraph[intervalRange + i];
        }
        
        AddValues(intervalValues);
        DrawGraph();
    }

    public void ShiftLeft()
    {
        if (currentPartOfShiftingGraph == 0)
        {
            return;
        }

        currentPartOfShiftingGraph--;

        Vector2[] intervalValues = new Vector2[intervalInShiftingGraph];

        int intervalRange = intervalInShiftingGraph * currentPartOfShiftingGraph;

        for (int i = 0; i < intervalInShiftingGraph; i++)
        {
            intervalValues[i] = allValuesForShiftingGraph[intervalRange + i];
        }

        AddValues(intervalValues);
        DrawGraph();
    }

    private void ResetGraph()
    {
        lineRendererPositions = null;

        xAxis.localPosition = Vector3.zero;
        yAxis.localPosition = Vector3.zero;

        if (temporaryObjects.Count == 0)
            return;

        for (int i = 0; i < temporaryObjects.Count; i++)
        {
            Destroy(temporaryObjects[i].gameObject);
        }

        temporaryObjects.Clear();
    }

    private void AdjustCoordinates()
    {
        float maxX = 0;
        float maxY = 0;
        float minX = 0;
        float minY = 0;
        
        for (int i = 0; i < currentValues.Length; i++)
        {
            float x = currentValues[i].x;
            float y = currentValues[i].y;

            if (x > 0)
            {
                if (x > maxX)
                {
                    maxX = x;
                }
            }

            if (y > 0)
            {
                if (y > maxY)
                {
                    maxY = y;
                }
            }

            if (x < 0)
            {
                if (x < minX)
                {
                    minX = x;
                }
            }

            if (y < 0)
            {
                if (y < minY)
                {
                    minY = y;
                }
            }
        }

        float xAxisZoneSize = axisZone.sizeDelta.x - borderOffset;
        float yAxisZoneSize = axisZone.sizeDelta.y - borderOffset;
        
        float xAmplitude = maxX + Mathf.Abs(minX);
        float yAmplitude = maxY + Mathf.Abs(minY);

        float xScaleFactor = xAxisZoneSize / xAmplitude;
        float yScaleFactor = yAxisZoneSize / yAmplitude;

        Vector2 originCoordinates = new Vector2(xAxisZoneSize / 2, yAxisZoneSize / 2);
        float xReplaceFactor = GetReplaceFactor(maxX, minX, originCoordinates.x, xScaleFactor);
        float yReplaceFactor = GetReplaceFactor(maxY, minY, originCoordinates.y, yScaleFactor);

        xAxis.localPosition = new Vector3(xAxis.localPosition.x, xAxis.localPosition.y + yReplaceFactor, xAxis.localPosition.z);
        yAxis.localPosition = new Vector3(yAxis.localPosition.x + xReplaceFactor, yAxis.localPosition.y, yAxis.localPosition.z);

        DrawAxisMarks(new Vector2(maxX, maxY), new Vector2(minX, minY));

        if (lineRendererPositions == null)
        {
            lineRendererPositions = new Vector3[currentValues.Length];

            for (int i = 0; i < currentValues.Length; i++)
            {
                lineRendererPositions[i] = new Vector3((i + 1) * xMarkOffset + yAxis.localPosition.x, currentValues[i].y * yScaleFactor + yReplaceFactor, 0);
            }
        }
        else
        {
            for (int i = 0; i < currentValues.Length; i++)
            {
                lineRendererPositions[i] = new Vector3(currentValues[i].x * xScaleFactor + xReplaceFactor, currentValues[i].y * yScaleFactor + yReplaceFactor, 0);
            }
        }
    }

    private float GetReplaceFactor(float maxCoordinate, float minCoordinate, float origin, float scaleFactor)
    {
        float replaceFactor = 0;

        if (maxCoordinate > Mathf.Abs(minCoordinate))
        {
            replaceFactor = -origin + Mathf.Abs(minCoordinate * scaleFactor);
        }
        else if (maxCoordinate < Mathf.Abs(minCoordinate))
        {
            replaceFactor = origin - Mathf.Abs(maxCoordinate * scaleFactor);
        }

        return replaceFactor;
    }

    private void DrawAxisMarks(Vector2 maxValues, Vector2 minValues)
    {
        List<float> xUniqueValues = new List<float>();
        List<float> yUniqueValues = new List<float>();

        for (int i = 0; i < currentValues.Length; i++)
        {
            xUniqueValues.Add(currentValues[i].x);
            yUniqueValues.Add(currentValues[i].y);
        }

        (int xPositiveCount, int xNegativeCount) = SortBySign(xUniqueValues);
        (int yPositiveCount, int yNegativeCount) = SortBySign(yUniqueValues);

        int xMaxMarkCount = Mathf.Max(xPositiveCount, xNegativeCount);
        int yMaxMarkCount = Mathf.Max(yPositiveCount, yNegativeCount);

        float xPositiveAxisSize = ((axisZone.sizeDelta.x - borderOffset) / 2) - yAxis.localPosition.x;
        float xNegativeAxisSize = axisZone.sizeDelta.x - borderOffset - xPositiveAxisSize;
        float yPositiveAxisSize = ((axisZone.sizeDelta.y - borderOffset) / 2) - xAxis.localPosition.y;
        float yNegativeAxisSize = axisZone.sizeDelta.y - borderOffset - yPositiveAxisSize;

        float xAxisMaxPart = Mathf.Max(xPositiveAxisSize, xNegativeAxisSize);
        float yAxisMaxPart = Mathf.Max(yPositiveAxisSize, yNegativeAxisSize);

        xMarkOffset = xAxisMaxPart / xMaxMarkCount;
        yMarkOffset = yAxisMaxPart / yMaxMarkCount;

        float minMarkOffset = GetMinMarkOffset();

        if (yMarkOffset < minMarkOffset)
        {
            yMarkOffset = minMarkOffset;
        }    

        xPositiveCount = Mathf.RoundToInt(xPositiveAxisSize / xMarkOffset);
        xNegativeCount = Mathf.RoundToInt(xNegativeAxisSize / xMarkOffset);
        yPositiveCount = Mathf.RoundToInt(yPositiveAxisSize / yMarkOffset);
        yNegativeCount = Mathf.RoundToInt(yNegativeAxisSize / yMarkOffset);

        float xScaleGraduate = (maxValues.x - minValues.x) / (xPositiveCount + xNegativeCount);

        if (isXMarkSignatureAdjust)
        {
            InstantiateMarks(xAxisMarkPrefab, xPositiveCount, xMarkOffset, yAxis.localPosition.x, xAxis, xValueAdditionalSignature, xScaleGraduate, SetMarkPositionOnAxisX);
            InstantiateMarks(xAxisMarkPrefab, xNegativeCount, -xMarkOffset, yAxis.localPosition.x, xAxis, xValueAdditionalSignature, -xScaleGraduate, SetMarkPositionOnAxisX);
        }
        else
        {
            float[] signature = new float[currentValues.Length]; 
            
            for (int i = 0; i < currentValues.Length; i++)
            {
                signature[i] = currentValues[i].x;
            }
            
            InstantiateMarksWithoutAdjustingSignature(xAxisMarkPrefab, xPositiveCount, xMarkOffset, yAxis.localPosition.x, xAxis, xValueAdditionalSignature, signature, "", SetMarkPositionOnAxisX);
            InstantiateMarksWithoutAdjustingSignature(xAxisMarkPrefab, xNegativeCount, -xMarkOffset, yAxis.localPosition.x, xAxis, xValueAdditionalSignature, signature, "-", SetMarkPositionOnAxisX);
        }

        float yScaleGraduate = (maxValues.y - minValues.y) / (yPositiveCount + yNegativeCount);

        InstantiateMarks(yAxisMarkPrefab, yPositiveCount, yMarkOffset, xAxis.localPosition.y, yAxis, yValueAdditionalSignature, yScaleGraduate, SetMarkPositionOnAxisY);
        InstantiateMarks(yAxisMarkPrefab, yNegativeCount, -yMarkOffset, xAxis.localPosition.y, yAxis, yValueAdditionalSignature, -yScaleGraduate, SetMarkPositionOnAxisY);
    }

    private (int positive, int negative) SortBySign(List<float> _values)
    {
        int positiveCount = 0;
        int negativeCount = 0;

        for (int i = 0; i < _values.Count; i++)
        {
            float value = _values[i];

            if (value > 0)
            {
                positiveCount++;
            }
            else if (value < 0)
            {
                negativeCount++;
            }
        }

        return (positiveCount, negativeCount);
    }

    private float GetMinMarkOffset()
    {
        float yMinMarkOffset = axisZone.sizeDelta.y * minPartOfAxisForMarkOffset;

        return yMinMarkOffset;
    }

    private void InstantiateMarks(RectTransform axisMarkPrefab, int markCount, float markOffset, float replacingOtherAxisFromZero, 
        RectTransform axis, string valueAdditionalSignature, float scaleGraduate, Action <RectTransform, float> onCreatedMark)
    {
        for (int i = 0; i < markCount; i++)
        {
            RectTransform axisMark = Instantiate(axisMarkPrefab, axis);

            axisMark.GetComponent<AxisMark>().SetMarkValue((Mathf.RoundToInt(scaleGraduate) * (i + 1)).ToString() + valueAdditionalSignature);

            temporaryObjects.Add(axisMark);

            onCreatedMark.Invoke(axisMark, markOffset * (i + 1) + replacingOtherAxisFromZero);
        }
    }

    private void InstantiateMarksWithoutAdjustingSignature(RectTransform axisMarkPrefab, int markCount, float markOffset, float replacingOtherAxisFromZero,
        RectTransform axis, string valueAdditionalSignature, float[] signature, string signBeforeSignature, Action<RectTransform, float> onCreatedMark)
    {
        for (int i = 0; i < markCount; i++)
        {
            RectTransform axisMark = Instantiate(axisMarkPrefab, axis);

            axisMark.GetComponent<AxisMark>().SetMarkValue(signBeforeSignature + signature[i].ToString() + valueAdditionalSignature);

            temporaryObjects.Add(axisMark);

            onCreatedMark.Invoke(axisMark, markOffset * (i + 1) + replacingOtherAxisFromZero);
        }
    }

    private void SetMarkPositionOnAxisX(RectTransform mark, float markOffset)
    {
        mark.localPosition = new Vector3(markOffset, 0, 0);
    }

    private void SetMarkPositionOnAxisY(RectTransform mark, float markOffset)
    {
        mark.localPosition = new Vector3(0, markOffset, 0);
    }
}