﻿using UnityEngine;
using UnityEngine.UI;

public class TaskTextViewer : MonoBehaviour
{
    [SerializeField] private Tasks tasks;
    [SerializeField] private Text taskText;


    private void Start()
    {
        tasks.OnTaskTextChanging += ChangeText;
    }

    private void ChangeText(string text)
    {
        taskText.text = text;
    }
}
