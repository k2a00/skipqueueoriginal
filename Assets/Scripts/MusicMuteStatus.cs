﻿using UnityEngine;

public class MusicMuteStatus : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private GameObject musicOffImage;

    public void ChangeMuteState()
    {
        audioSource.mute = !audioSource.mute;

        musicOffImage.SetActive(!musicOffImage.activeSelf);
    }

    public bool GetCurrentMuteState()
    {
        return audioSource.mute;
    }
}
