﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonsLayout : MonoBehaviour
{
    [SerializeField] private GridLayoutGroup gridLayout;
    [SerializeField] private RectTransform rectTransform;

    [SerializeField] private Vector2 cellSizeLandscapeDefault;
    [SerializeField] private Vector2 rectLandscapeDefault;
    [SerializeField] private Vector2 cellSizePortraitDefault;
    [SerializeField] private Vector2 rectPortraitDefault;
    [SerializeField] private int bottomPortraitDefault;
    [SerializeField] private int bottomLandscapeDefault;



    public void SetLandscapeView()
    {       
        float width = rectTransform.rect.width;
        float height = rectTransform.rect.height;
            
        float x = (width * cellSizeLandscapeDefault.x) / rectLandscapeDefault.x;
        float y = (height * cellSizeLandscapeDefault.y) / rectLandscapeDefault.y;
        gridLayout.cellSize = new Vector2(x, y);

        gridLayout.padding.bottom = Mathf.RoundToInt((bottomLandscapeDefault * height) / rectLandscapeDefault.y);

        gridLayout.startAxis = GridLayoutGroup.Axis.Vertical;
        gridLayout.constraint = GridLayoutGroup.Constraint.FixedColumnCount;
    }

    public void SetPortraitView()
    {
        float width = rectTransform.rect.width;
        float height = rectTransform.rect.height;

        float x = (width * cellSizePortraitDefault.x) / rectPortraitDefault.x;
        float y = (height * cellSizePortraitDefault.y) / rectPortraitDefault.y;
        gridLayout.cellSize = new Vector2(x, y);

        gridLayout.padding.bottom = Mathf.RoundToInt((bottomPortraitDefault * height) / rectPortraitDefault.y);

        gridLayout.startAxis = GridLayoutGroup.Axis.Horizontal;
        gridLayout.constraint = GridLayoutGroup.Constraint.FixedRowCount;
    }
}
