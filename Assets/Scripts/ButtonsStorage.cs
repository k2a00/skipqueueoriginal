﻿using System.Collections.ObjectModel;
using System.Collections.Generic;
using UnityEngine;

public class ButtonsStorage : MonoBehaviour
{
    [SerializeField] private List<BinaryButton> binaryButtons;
    public ReadOnlyCollection<BinaryButton> BinaryButtons { get { return binaryButtons.AsReadOnly(); } }
    
    [SerializeField] private int minGoodButtons;
    [SerializeField] private RulesWatcher rulesWatcher;
    

    public void SetDefaultValues()
    {
        for (int i = 0; i < binaryButtons.Count; i++)
        {
            binaryButtons[i].SetDefaultValues();
        }
    }

    public void PrepareToRound()
    {
        for (int i = 0; i < binaryButtons.Count; i++)
        {
            binaryButtons[i].PrepareToRound();
        }

        SetGoodStates();
    }

    public void SetUninteractableMode()
    {
        for (int i = 0; i < binaryButtons.Count; i++)
        {
            binaryButtons[i].SetUninteractableMode();
        }
    }

    public void ChangeButtonsSprite(Sprite sprite)
    {
        for (int i = 0; i < binaryButtons.Count; i++)
        {
            binaryButtons[i].SetSkin(sprite);
        }
    }

    private void SetGoodStates()
    {
        int maxGoodButtons = Random.Range(minGoodButtons, binaryButtons.Count);

        List<BinaryButton> temporary = new List<BinaryButton>();

        for (int i = 0; i < binaryButtons.Count; i++)
        {
            temporary.Add(binaryButtons[i]);
        }
        
        for (int i = 0; i < maxGoodButtons; i++)
        {
            int currentIndex = Random.Range(0, temporary.Count);

            temporary[currentIndex].SetGoodState();
            temporary.RemoveAt(currentIndex);
        }

        rulesWatcher.SetMaxGoodButtonsCount(maxGoodButtons);
    }
}
