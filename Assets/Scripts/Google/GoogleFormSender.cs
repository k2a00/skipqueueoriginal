﻿using System.Collections;
using UnityEngine;

public class GoogleFormSender : MonoBehaviour
{
    public void SendData(string url, ConfigForFormSending[] configs)
    {
        StartCoroutine(Send(url, configs));   
    }

    private IEnumerator Send(string url, ConfigForFormSending[] configs)
    {
        WWWForm form = new WWWForm();

        for (int i = 0; i < configs.Length; i++)
        {
            form.AddField(configs[i].EntryID, configs[i].Value);
        }

        byte[] rawData = form.data;
        WWW www = new WWW(url, rawData);

        yield return www;
    }
}
