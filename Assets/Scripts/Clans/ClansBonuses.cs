﻿using System;
using UnityEngine;

public class ClansBonuses : MonoBehaviour
{
    [SerializeField] private ReceivedPointsCounter receivedPointsCounter;
    [SerializeField] private Clans clans;
    [SerializeField] private BelongingToClan belongingToClan;

    public event Action<bool, int> OnBonusesChanged;

    private readonly int[] bonusForFirstDetection = new int[] { 15, 10, 0 };


    private void Start()
    {
        clans.OnClansRatingChanged += ChangeClanBonus;
        belongingToClan.OnClanStateChange += ChangeClanBonus;
    }

    private void ChangeClanBonus()
    {
        bool needReduceFirstly = true;
        
        if (belongingToClan.SelectedClan == Clans.ClansNames.None)
        {
            OnBonusesChanged?.Invoke(needReduceFirstly, 0);

            return;
        }

        int playerClan = (int)belongingToClan.SelectedClan;
        int playerClanRatingPlace = 0;

        for (int i = 0; i < clans.ClanRating.Length; i++)
        {
            if (clans.ClanRating[i] == playerClan)
            {
                playerClanRatingPlace = i;

                break;
            }
        }

        if (playerClanRatingPlace != 1)
        {
            needReduceFirstly = false;
        }

        OnBonusesChanged?.Invoke(needReduceFirstly, bonusForFirstDetection[playerClanRatingPlace]);
    }
}
