﻿using UnityEngine.UI;


[System.Serializable]
public class Vocabulary
{
    public Text[] TextPlace;
    public string[] Equivalents;
}


[System.Serializable]
public class EventVocabulary
{
    public Text TextPlace;
    public EquivalentCouple[] Equivalents;
}


[System.Serializable]
public class EquivalentCouple
{
    public string[] Equivalents;
}