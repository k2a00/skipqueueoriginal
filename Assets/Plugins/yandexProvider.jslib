mergeInto(LibraryManager.library, {

  Auth: function() {
    auth();
  },
  
  AuthInGame: function() {
    authInGame();
  },
  
  SaveDataToAllStorage: function(data){
    saveDataToAllStorage(UTF8ToString(data));
  },

  SaveDataToLocalStorage: function(data){
    saveDataToLocalStorage(UTF8ToString(data));
  },

  ShowCommonADV: function () {
    showFullscrenAd();
  },

  ShowRewardADV: function(id) {
    showRewardedAd(id);
  },
  
  GetLeaderBoardEntries: function(tableName){
    getLeaderboard(UTF8ToString(tableName));
  },
  
  SetLeaderboardScore: function(tableName, score){
    setLeaderboardScore(UTF8ToString(tableName), score);
  }

});