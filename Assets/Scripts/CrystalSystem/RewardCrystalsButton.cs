﻿using System;
using UnityEngine;

public class RewardCrystalsButton : MonoBehaviour
{
    [SerializeField] private BelongingToClan belongingToClan;
    [SerializeField] private Clans clans;
    [SerializeField] private CrystalAccountant crystalAccountant;
    [SerializeField] private GameObject view;

    public event Action OnNeedSaveData;
    public int RewardCrystalHour { get; private set; }
    public int ADVShowed { get; private set; } = 0;

    private int currentHour;
    private int availibleADV = 1;

    private readonly int idRewardADV = 2;
    private readonly string[] NOTICE = new string[] { "Watch ads and get a bonus!", "Посмотри рекламу и получи бонус!",
        "Reklamları izleyin ve bonus kazanın!" };


    private void Start()
    {
        YandexSDK.Instance.OnRewardADVShowed += AddCrystal;
    }

    public void SetHour(int hour)
    {
        currentHour = hour;
    }

    public void LoadData(int rewardCrystalHour, int aDVShowed)
    {
        if (currentHour != rewardCrystalHour)
        {
            RewardCrystalHour = currentHour;

            ADVShowed = 0;
        }
        else
        {
            RewardCrystalHour = rewardCrystalHour;
            ADVShowed = aDVShowed;
        }

        CountAvailibleADV();
    }

    public bool CheckADVAvalible() => ADVShowed <= availibleADV;

    public void ShowADVNotice()
    {
        MessageBlock.Instance.ShowMessage(NOTICE[YandexSDK.Instance.LanguageIndex], ShowAdVideo);
    }

    private void ShowAdVideo()
    {
        YandexSDK.Instance.ShowRewardAdvertisment(idRewardADV);
    }

    private void AddCrystal(int id)
    {
        if (id != idRewardADV)
        {
            return;
        }

        crystalAccountant.Increase();

        ADVShowed++;
        
        view.SetActive(CheckADVAvalible());

        OnNeedSaveData?.Invoke();
    }

    private void CountAvailibleADV()
    {
        if (belongingToClan.SelectedClan != Clans.ClansNames.None)
        {
            int[] rating = clans.ClanRating;

            for (int i = 0; i < rating.Length; i++)
            {
                if (rating[i] == (int)belongingToClan.SelectedClan)
                {
                    availibleADV = i + 1;
                    break;
                }
            }
        }
    }
}
