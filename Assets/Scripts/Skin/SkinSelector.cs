﻿using UnityEngine;

public class SkinSelector : MonoBehaviour
{
    [SerializeField] private SelectorView skinSelectorView;
    
    public int SelectedSkin { get; private set; }


    public void BecomeListener(SkinVariant skinVariant)
    {
        skinVariant.OnClickedOnskin += SelectSkin;
    }

    public void SelectSkin(int index, Transform parent)
    {
        SelectedSkin = index;

        skinSelectorView.ReplaceSelectorCenterMiddle(parent);
    }

    public void ReplaceSelector(Transform parent)
    {
        skinSelectorView.ReplaceSelectorCenterMiddle(parent);
    }
}
