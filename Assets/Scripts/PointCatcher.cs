﻿using UnityEngine;

public class PointCatcher : MonoBehaviour
{
    [SerializeField] private ScoreBoard scoreBoard;


    private void OnEnable()
    {
        scoreBoard.RefreshScoreBoard();        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<ReceivedPoints>())
        {
            scoreBoard.ShowNewScore(collision.GetComponent<ReceivedPoints>().ReceivedPointsValue);

            collision.gameObject.SetActive(false);
        }
    }
}
