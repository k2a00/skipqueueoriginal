﻿using System;
using UnityEngine;

public class JoiningClanUI : MonoBehaviour
{
    [SerializeField] private Clans.ClansNames clanName;
    public event Action<Clans.ClansNames> OnJoinClan;

    public void JoinClan()
    {
        OnJoinClan?.Invoke(clanName);
    }
}
