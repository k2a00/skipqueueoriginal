﻿using System.Collections;
using UnityEngine;

public class LeaderboardOrdersAfterAuth : MonoBehaviour
{
    [SerializeField] private Leaderboard afterMatchLeaderboard;
    [SerializeField] private Leaderboard maxPointsLeaderboard;
    [SerializeField] private Leaderboard donationsLeaderboard;

    [SerializeField] private BelongingToClan belongingToClan;

    private WaitForSeconds waitForSeconds = new WaitForSeconds(2f);

    private readonly string[] LEADERBOARDSNAMES = new string[] { "Alfa", "Beta", "Gamma" };


    private void Start()
    {
        YandexSDK.Instance.OnAuthInGame += OnAuthInGame;        
    }

    private void OnAuthInGame()
    {
        StartCoroutine(OrderLeaderboardsAfterAuthInGame());
    }

    private IEnumerator OrderLeaderboardsAfterAuthInGame()
    {
        yield return waitForSeconds;

        YandexSDK.Instance.OrderLeaderboardEntries("AfterMatchScore", afterMatchLeaderboard.CreateLeaderboard);
        
        yield return waitForSeconds;

        YandexSDK.Instance.OrderLeaderboardEntries("MaxPoints", maxPointsLeaderboard.CreateLeaderboard);
        
        yield return waitForSeconds;

        if (belongingToClan.SelectedClan != Clans.ClansNames.None)
        {
            YandexSDK.Instance.OrderLeaderboardEntries(LEADERBOARDSNAMES[(int)belongingToClan.SelectedClan - 1], donationsLeaderboard.CreateLeaderboard);
        }
    }
}
