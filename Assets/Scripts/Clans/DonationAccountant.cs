﻿using UnityEngine;

public class DonationAccountant : MonoBehaviour
{
    public int Donation { get; private set; } = 0;


    public void Increase(int value)
    {
        Donation += value;
    }

    public void ResetDonations()
    {
        Donation = 0;
    }

    public void LoadData(int donation)
    {
        Donation = donation;
    }
}
