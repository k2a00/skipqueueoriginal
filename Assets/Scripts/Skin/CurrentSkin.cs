﻿using UnityEngine;

public class CurrentSkin : MonoBehaviour
{
    [SerializeField] private ButtonsStorage buttonsStorage;
    [SerializeField] private SkinStorage skinStorage;
    [SerializeField] private SkinSelector skinSelector;

    public void SetCurrentSkin()
    {
        buttonsStorage.ChangeButtonsSprite(skinStorage.GetCurrentSkin(skinSelector.SelectedSkin));
    }
}
