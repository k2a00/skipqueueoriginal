﻿using UnityEngine;
using UnityEngine.UI;

public class ClanInformation : MonoBehaviour
{
    [SerializeField] private Clans.ClansNames clanName;
    [SerializeField] private Text ratingScore;


    public void SetRatingScore(int value)
    {
        ratingScore.text = value.ToString();
    }
}
