﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class BinaryButton : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private Image skin;
    [SerializeField] private Image background;
    [SerializeField] private Color goodBackground;
    [SerializeField] private Color badBackground;
    public delegate void PressButtonCallback(BinaryButton binaryButton);
    public event PressButtonCallback OnPressButton; 
    public bool WasPressInRound { get; private set; }

    private BinaryButtonState.State state;
    private Color defaultBackground = Color.white;
    private readonly float badBackgroandDelay = 0.6f;

    public void SetDefaultValues()
    {
        if (state == BinaryButtonState.State.GoodButton)
        {
            background.color = defaultBackground;
        }

        button.interactable = true;
    }

    public void SetUninteractableMode()
    {
        background.color = defaultBackground;

        button.interactable = false;
    }

    public void PrepareToRound()
    {
        SetDefaultValues();

        state = BinaryButtonState.State.BadButton;

        WasPressInRound = false;
    }

    public void SetGoodState()
    {
        state = BinaryButtonState.State.GoodButton;
    }

    public void PressButton()
    {
        if (state == BinaryButtonState.State.GoodButton)
        {
            background.color = goodBackground;

            button.interactable = false;
        }
        else
        {
            background.color = badBackground;

            StartCoroutine(ChangeBackgroundToDefault());
        }

        OnPressButton?.Invoke(this);

        if (!WasPressInRound)
        {
            WasPressInRound = true;
        }
    }

    public BinaryButtonState.State GetBinaryButtonState()
    {
        return state;
    }

    public void SetSkin(Sprite _skin)
    {
        skin.sprite = _skin;
    }

    private IEnumerator ChangeBackgroundToDefault()
    {
        yield return new WaitForSeconds(badBackgroandDelay);

        background.color = defaultBackground;
    }
}
