﻿using System;
using UnityEngine;


public class Notice : MonoBehaviour
{
    public Action<bool> OnClicked;

    public void AcceptOffer()
    {
        OnClicked?.Invoke(true);

        gameObject.SetActive(false);
    }

    public void RejectOffer()
    {
        OnClicked?.Invoke(false);

        gameObject.SetActive(false);
    }
}