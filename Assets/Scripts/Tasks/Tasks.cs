﻿using System;
using UnityEngine;

public class Tasks : MonoBehaviour
{
    public int MatchesNeed { get; private set; } = 0;
    public int TotalPointsNeed { get; private set; } = 0;
    public int PointsPerMatch { get; private set; } = 0;
    public int MatchesPlayed { get; private set; } = 0;
    public int PointsReceived { get; private set; } = 0;
    public string TaskText { get; private set; } = "";

    public event Action<string> OnTaskTextChanging;

    [SerializeField] private CrystalAccountant crystalAccountant;
    [SerializeField] private BelongingToClan belongingToClan;
    [SerializeField] private Clans clans;
    [SerializeField] private GameObject clanRoom;

    private int languageIndex;

    private readonly string[] pointsPrefix = new string[] { "Points:", "Очки:", "Puani:" };
    private readonly string[] gamesPrefix = new string[] { "Games", "Игр", "Oyunları" };
    private readonly string[] addition = new string[] { " where points", ", где очков", ". Oyun başına puan" };
    private readonly string[] crystalPostfix = new string[] { "crystal", "кристалл", "kristal" };

    private readonly string[] COMPLETED = new string[] { "The task is completed! +1 crystal. Do you want to support the clan?",
        "Задание выполнено! Вы получили кристалл. Открыть клановую комнату, чтобы поддержать клан?", 
        "Görev tamamlandı! +1 kristal. Klanı desteklemek mi istiyorsunuz?" };

    private readonly int[] pointsPerMatchClanRatingCorrection = new int[] { 1500, 800, 400 };

    private void Start()
    {
        YandexSDK.Instance.OnLanguageSet += SetLanguage;
    }

    public void LoadTaskValues(int matchesNeed, int totalPointsNeed, int pointsPerMatch, int matchesPlayed, int pointsReceived)
    {
        MatchesNeed = matchesNeed;
        TotalPointsNeed = totalPointsNeed;
        PointsPerMatch = pointsPerMatch;
        MatchesPlayed = matchesPlayed;
        PointsReceived = pointsReceived;

        if (matchesNeed == 0 && totalPointsNeed == 0)
        {
            CreateTask();
            return;
        }

        RefreshTaskText();
    }

    public void CheckExecution(int points)
    {
        if (MatchesNeed == 0 && TotalPointsNeed == 0)
        {
            return;
        }

        if (TotalPointsNeed != 0)
        {
            PointsReceived += points;

            RefreshTaskText();

            if (TotalPointsNeed > PointsReceived)
            {
                return;
            }
        }
        else if (PointsPerMatch != 0)
        {
            if (PointsPerMatch > points)
            {
                return;
            }

            MatchesPlayed++;
            
            RefreshTaskText();

            if (MatchesNeed > MatchesPlayed)
            {
                return;
            }
        }
        else
        {
            MatchesPlayed++;

            RefreshTaskText();

            if (MatchesNeed > MatchesPlayed)
            {
                return;
            }
        }

        CompleteTask();
    }

    private void CreateTask()
    {
        ResetValues();

        int variant = UnityEngine.Random.Range(0, 3);

        if (variant == 0)
        {
            MatchesNeed = UnityEngine.Random.Range(3, 6);
        }
        else if (variant == 1)
        {
            TotalPointsNeed = UnityEngine.Random.Range(60, 120) * 100;
        }
        else
        {
            MatchesNeed = UnityEngine.Random.Range(3, 6);
            PointsPerMatch = UnityEngine.Random.Range(20, 31) * 100 + GetClanRatingCorrection();
        }

        RefreshTaskText();
    }

    private void RefreshTaskText()
    {
        string reward = $"( +1 {crystalPostfix[languageIndex]} )";

        if (TotalPointsNeed != 0)
        {
            TaskText = $"* {pointsPrefix[languageIndex]} {PointsReceived}/{TotalPointsNeed} " + reward;
        }
        else if (PointsPerMatch != 0)
        {
            TaskText = $"* {gamesPrefix[languageIndex]} {MatchesPlayed}/{MatchesNeed}{addition[languageIndex]} {PointsPerMatch}+ " + reward;
        }
        else
        {
            TaskText = $"* {gamesPrefix[languageIndex]} {MatchesPlayed}/{MatchesNeed} " + reward;
        }

        OnTaskTextChanging?.Invoke(TaskText);
    }

    private void CompleteTask()
    {
        ResetValues();

        crystalAccountant.Increase();

        MessageBlock.Instance.ShowMessage(COMPLETED[languageIndex], ShowClanRoom);

        CreateTask();
    }

    private void ResetValues()
    {
        MatchesNeed = 0;
        TotalPointsNeed = 0;
        PointsPerMatch = 0;
        MatchesPlayed = 0;
        PointsReceived = 0;
        TaskText = "";
    }

    private void SetLanguage()
    {
        languageIndex = YandexSDK.Instance.LanguageIndex;
    }

    private void ShowClanRoom()
    {
        clanRoom.SetActive(true);
    }

    private int GetClanRatingCorrection()
    {
        if (belongingToClan.SelectedClan == Clans.ClansNames.None)
        {
            return 0;
        }

        int playerClan = (int)belongingToClan.SelectedClan;
        int playerClanRatingPlace = 0;

        for (int i = 0; i < clans.ClanRating.Length; i++)
        {
            if (clans.ClanRating[i] == playerClan)
            {
                playerClanRatingPlace = i;

                break;
            }
        }

        return pointsPerMatchClanRatingCorrection[playerClanRatingPlace];
    }
}
