﻿using System.Collections;
using UnityEngine;

public class Metrics : MonoBehaviour
{
    [SerializeField] private GoogleFormSender googleFormSender;
    [SerializeField] private CrystalAccountant crystalAccountant;
    [SerializeField] private BelongingToClan belongingToClan;
    [SerializeField] private DonationAccountant donationAccountant;
    [SerializeField] private Statistics statistics;

    private readonly string formURL = "https://docs.google.com/forms/u/0/d/e/1FAIpQLSd6WQn3zCwKiZBOyb_qf3yK2u3lgBIvL5Y1fLfJs_eg_xISJg/formResponse";
    private readonly string crystalReceivedEntry = "entry.1218495073";
    private readonly string crystalSpentEntry = "entry.1135031424";
    private readonly string clanNumberEntry = "entry.1985287331";
    private readonly string donationEntry = "entry.94474815";
    private readonly string maxPointEntry = "entry.759530032";
    private readonly string matchCountEntry = "entry.189240736";
    private readonly string languageIndex = "entry.922268707";
    private readonly string playerID = "entry.132922781";


    private void Start()
    {
        StartCoroutine(Wait());
    }

    private IEnumerator Wait()
    {
        yield return new WaitForSeconds(30);

        Send();
    }

    private void Send()
    {
        ConfigForFormSending[] configs = new ConfigForFormSending[]
        {
            new ConfigForFormSending()
            {
                EntryID = crystalReceivedEntry,
                Value = crystalAccountant.CrystalReceived.ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = crystalSpentEntry,
                Value = crystalAccountant.CrystalSpent.ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = clanNumberEntry,
                Value = ((int)belongingToClan.SelectedClan).ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = donationEntry,
                Value = donationAccountant.Donation.ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = maxPointEntry,
                Value = statistics.MaxPoints.ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = matchCountEntry,
                Value = statistics.MatchCount.ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = languageIndex,
                Value = YandexSDK.Instance.LanguageIndex.ToString(),
            },
            new ConfigForFormSending()
            {
                EntryID = playerID,
                Value = YandexSDK.Instance.PlayerUniqueID,
            }
        };

        googleFormSender.SendData(formURL, configs);
    }
}
