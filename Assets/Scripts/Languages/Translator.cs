﻿using System.Collections.Generic;
using UnityEngine;

public class Translator : MonoBehaviour
{
    [SerializeField] private List<Vocabulary> vocabularies;
    [SerializeField] private Vocabulary currentVocabulary;
    private int currentIndex = 0;

    private void OnEnable()
    {
        YandexSDK.Instance.OnLanguageSet += SetCorrectLanguage;
    }

    public void SetCorrectLanguage()
    {
        int languageIndex = YandexSDK.Instance.LanguageIndex;

        for (int i = 0; i < vocabularies.Count; i++)
        {
            for (int a = 0; a < vocabularies[i].TextPlace.Length; a++)
            {
                vocabularies[i].TextPlace[a].text = vocabularies[i].Equivalents[languageIndex];
            }
        }
    }

    #region EDITOR
    public void AddObject()
    {
        if (vocabularies == null)
            vocabularies = new List<Vocabulary>();

        vocabularies.Add(currentVocabulary);
    }

    public void AddEmpty()
    {
        if (vocabularies == null)
            vocabularies = new List<Vocabulary>();

        currentVocabulary = new Vocabulary
        {
            Equivalents = new string[2]
        };
        currentIndex = vocabularies.Count - 1;
    }

    public Vocabulary GetNext()
    {
        if (currentIndex < vocabularies.Count - 1)
            currentIndex++;

        currentVocabulary = this[currentIndex];
        return currentVocabulary;
    }

    public Vocabulary GetPrevious()
    {
        if (currentIndex > 0)
            currentIndex--;

        currentVocabulary = this[currentIndex];
        return currentVocabulary;
    }

    public void RemoveCurrentObject()
    {
        if (currentIndex > 0)
        {
            currentVocabulary = vocabularies[--currentIndex];
            vocabularies.RemoveAt(++currentIndex);
        }
        else
        {
            vocabularies.Clear();
            currentVocabulary = null;
        }
    }


    public Vocabulary this[int index]
    {
        get
        {
            if (vocabularies != null && index >= 0 && index < vocabularies.Count)
                return vocabularies[index];
            return null;
        }

        set
        {
            if (vocabularies == null)
                vocabularies = new List<Vocabulary>();

            if (index >= 0 && index < vocabularies.Count && value != null)
                vocabularies[index] = value;
            else Debug.Log("Выход за границы массива или переданное значение = null");
        }
    }
    #endregion
}