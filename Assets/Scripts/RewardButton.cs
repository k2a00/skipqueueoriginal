﻿using UnityEngine;

public class RewardButton : MonoBehaviour
{
    [SerializeField] private GameObject[] rewardButtons;
    [Space]
    [SerializeField] private Notice notice;
    [SerializeField] private GameTimer matchTimer;
    [SerializeField] private GameTimer changingRoundTimer;
    [SerializeField] private RulesWatcher rulesWatcher;

    private readonly int idRewardADV = 1; 

    private void OnEnable()
    {
        notice.OnClicked += CheckPlayerDecision;
        YandexSDK.Instance.OnRewardADVShowed += DestroyButton;
    }

    public void ShowNotice()
    {
        matchTimer.Pause();
        changingRoundTimer.Pause();

        notice.gameObject.SetActive(true);
    }

    private void DestroyButton(int id)
    {
        if (id != idRewardADV)
        {
            return;
        }

        for (int i = 0; i < rewardButtons.Length; i++)
        {
            Destroy(rewardButtons[i]);
        }
    }

    private void CheckPlayerDecision(bool decision)
    {
        if (decision)
        {
            YandexSDK.Instance.ShowRewardAdvertisment(idRewardADV);
        }
        else 
        {
            rulesWatcher.PrepareToChangingRound();
        }
    }

    private void OnDisable()
    {
        notice.OnClicked -= CheckPlayerDecision;
        YandexSDK.Instance.OnRewardADVShowed -= DestroyButton;
    }
}
