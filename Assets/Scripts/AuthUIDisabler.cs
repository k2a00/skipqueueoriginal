﻿using UnityEngine;

public class AuthUIDisabler : MonoBehaviour
{
    [SerializeField] private GameObject[] authUI;

    private void Start()
    {
        YandexSDK.Instance.OnAuthenticated += DisableAuthUI;
    }

    private void DisableAuthUI()
    {
        for (int i = 0; i < authUI.Length; i++)
        {
            authUI[i].SetActive(false);
        }
    }
}
