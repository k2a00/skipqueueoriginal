﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Statistics : MonoBehaviour
{
    [SerializeField] private ScoreBoard scoreBoard;
    [SerializeField] private ReceivedPointsCounter receivedPointsCounter;
    [SerializeField] private CrystalAccountant crystalAccountant;
    [SerializeField] private Tasks tasks;
    [SerializeField] private BelongingToClan belongingToClan;
    [SerializeField] private ClanSupport clanSupport;
    [SerializeField] private Clans clans;
    [SerializeField] private RewardCrystalsButton rewardCrystalsButton;
    [SerializeField] private Leaderboard maxPointsLeaderboard;
    [SerializeField] private DonationAccountant donationAccountant;
    [SerializeField] private GameObject leaderboardView;

    public int MaxPoints { get; private set; } = 0;
    public int MatchCount { get; private set; } = 0;
    public Action OnDataChecked;

    private int[] statistics = new int[30];
    private int currentResult = 0;

    private readonly string[] MAXPOINTS = new string[] { "A new record has been set! Do you want to see the leaderboard?",
        "Новый рекорд установлен! Показать таблицу лидеров?", "Yeni bir rekor kırıldı! Size göstermek için bir tablo liderler?" };
    private readonly string[] LEADERBOARDSNAMES = new string[] { "Alfa", "Beta", "Gamma" };


    private void Start()
    {
        YandexSDK.Instance.OnLocalDataGot += LoadData;
        YandexSDK.Instance.OnDataFromAccountGot += CheckDataAfterAuth;

        belongingToClan.OnClanStateChange += SaveData;
        clanSupport.OnNeedSaveData += SaveData;
        rewardCrystalsButton.OnNeedSaveData += SaveData;

        for (int i = 0; i < statistics.Length; i++)
        {
            statistics[i] = 1;
        }
    }

    public void AddMatchStatistics()
    {
        int lengthArray = statistics.Length;
        int points = scoreBoard.Points;

        currentResult = points;

        if (MatchCount < lengthArray)
        {
            statistics[MatchCount] = points;
        }
        else
        {
            for (int i = 0; i < lengthArray - 1; i++)
            {
                statistics[i] = statistics[i + 1];
            }

            statistics[lengthArray - 1] = points;
        }

        MatchCount++;

        tasks.CheckExecution(points);

        int leaderboardResult = points;
        
        if (leaderboardResult < 0)
        {
            leaderboardResult = 0;
        }
            
        YandexSDK.Instance.SetLeaderBoardScore("AfterMatchScore", leaderboardResult);

        if (MaxPoints < points)
        {
            MaxPoints = points;

            YandexSDK.Instance.SetLeaderBoardScore("MaxPoints", points);

            StartCoroutine(ShowMessage());
        }

        SaveData();
    }

    public float[] GetPointsForMatches()
    {
        List<float> temporary = new List<float>();

        for (int i = 0; i < statistics.Length; i++)
        {
            if (statistics[i] != 1)
            {
                temporary.Add(statistics[i]);
            }
        }

        float[] points = new float[temporary.Count];
        
        for (int i = 0; i < temporary.Count; i++)
        {
            points[i] = temporary[i];
        }

        return points;
    }

    private IEnumerator ShowMessage()
    {
        yield return new WaitForSeconds(2f);

        MessageBlock.Instance.ShowMessage(MAXPOINTS[YandexSDK.Instance.LanguageIndex], OrderLeaderboard);
    }

    private void OrderLeaderboard()
    {
        leaderboardView.SetActive(true);

        maxPointsLeaderboard.OrderLeaderboard("MaxPoints");
    }

    private void LoadData()
    {
        UserGameData gameData = YandexSDK.Instance.GetUserGameData;

        if (gameData.MaxPoint != 0 || gameData.ClanNumber != 0)
        {
            crystalAccountant.LoadCrystalCount(gameData.CrystalReceived, gameData.CrystalSpent);
        }

        tasks.LoadTaskValues(gameData.MatchesNeed, gameData.TotalPointsNeed, gameData.PointsPerMatch, 
            gameData.MatchesPlayed, gameData.PointsReceived);

        belongingToClan.LoadData(gameData.ClanNumber);

        clans.LoadData(gameData.RatingLoadingDay, new int[] { gameData.Clan1Score, gameData.Clan2Score, gameData.Clan3Score });

        rewardCrystalsButton.LoadData(gameData.RewardCrystalHour, gameData.ADVShowed);

        donationAccountant.LoadData(gameData.Donation);

        LoadMatchCount(gameData.MatchCount);

        statistics[0] = gameData.result1;
        statistics[1] = gameData.result2;
        statistics[2] = gameData.result3;
        statistics[3] = gameData.result4;
        statistics[4] = gameData.result5;
        statistics[5] = gameData.result6;
        statistics[6] = gameData.result7;
        statistics[7] = gameData.result8;
        statistics[8] = gameData.result9;
        statistics[9] = gameData.result10;
        statistics[10] = gameData.result11;
        statistics[11] = gameData.result12;
        statistics[12] = gameData.result13;
        statistics[13] = gameData.result14;
        statistics[14] = gameData.result15;
        statistics[15] = gameData.result16;
        statistics[16] = gameData.result17;
        statistics[17] = gameData.result18;
        statistics[18] = gameData.result19;
        statistics[19] = gameData.result20;
        statistics[20] = gameData.result21;
        statistics[21] = gameData.result22;
        statistics[22] = gameData.result23;
        statistics[23] = gameData.result24;
        statistics[24] = gameData.result25;
        statistics[25] = gameData.result26;
        statistics[26] = gameData.result27;
        statistics[27] = gameData.result28;
        statistics[28] = gameData.result29;
        statistics[29] = gameData.result30;

        MaxPoints = gameData.MaxPoint;
    }

    private void SaveData()
    {
        UserGameData userGameData = new UserGameData();

        userGameData.CrystalReceived = crystalAccountant.CrystalReceived;
        userGameData.CrystalSpent = crystalAccountant.CrystalSpent;
        
        userGameData.MatchesNeed = tasks.MatchesNeed;
        userGameData.TotalPointsNeed = tasks.TotalPointsNeed;
        userGameData.PointsPerMatch = tasks.PointsPerMatch;
        userGameData.MatchesPlayed = tasks.MatchesPlayed;
        userGameData.PointsReceived = tasks.PointsReceived;

        userGameData.ClanNumber = (int)belongingToClan.SelectedClan;

        userGameData.RatingLoadingDay = clans.DayRatingUpdate;
        userGameData.Clan1Score = clans.ClansScore[0];
        userGameData.Clan2Score = clans.ClansScore[1];
        userGameData.Clan3Score = clans.ClansScore[2];

        userGameData.RewardCrystalHour = rewardCrystalsButton.RewardCrystalHour;
        userGameData.ADVShowed = rewardCrystalsButton.ADVShowed;

        userGameData.Donation = donationAccountant.Donation;

        userGameData.MaxPoint = MaxPoints;

        userGameData.MatchCount = MatchCount;

        userGameData.result1 = statistics[0];
        userGameData.result2 = statistics[1];
        userGameData.result3 = statistics[2];
        userGameData.result4 = statistics[3];
        userGameData.result5 = statistics[4];
        userGameData.result6 = statistics[5];
        userGameData.result7 = statistics[6];
        userGameData.result8 = statistics[7];
        userGameData.result9 = statistics[8];
        userGameData.result10 = statistics[9];
        userGameData.result11 = statistics[10];
        userGameData.result12 = statistics[11];
        userGameData.result13 = statistics[12];
        userGameData.result14 = statistics[13];
        userGameData.result15 = statistics[14];
        userGameData.result16 = statistics[15];
        userGameData.result17 = statistics[16];
        userGameData.result18 = statistics[17];
        userGameData.result19 = statistics[18];
        userGameData.result20 = statistics[19];
        userGameData.result21 = statistics[20];
        userGameData.result22 = statistics[21];
        userGameData.result23 = statistics[22];
        userGameData.result24 = statistics[23];
        userGameData.result25 = statistics[24];
        userGameData.result26 = statistics[25];
        userGameData.result27 = statistics[26];
        userGameData.result28 = statistics[27];
        userGameData.result29 = statistics[28];
        userGameData.result30 = statistics[29];

        YandexSDK.Instance.SaveDataToStorages(JsonUtility.ToJson(userGameData));
    }

    private void LoadMatchCount(int count)
    {
        MatchCount = count;

        if (MatchCount == 0)
        {
            for (int i = 0; i < statistics.Length; i++)
            {
                statistics[i] = 1;
            }

            return;
        }
    }

    private void CheckDataAfterAuth()
    {
        UserGameData accountSave = YandexSDK.Instance.GetUserGameData;

        int accountMatchCount = accountSave.MatchCount;

        if (accountMatchCount == 0)
        {
            SaveData();

            return;
        }

        if (MatchCount == 0 && accountMatchCount > 0)
        {
            LoadData();

            return;
        }

        int localMatchCount = MatchCount;
        int statisticsLength = statistics.Length;
        List<int> validLocalSave = new List<int>();

        for (int i = 0; i < statisticsLength; i++)
        {
            if (statistics[i] != 1)
            {
                validLocalSave.Add(statistics[i]);
            }
        }

        int localCrystalReceived = crystalAccountant.CrystalReceived;
        int localCrystalSpent = crystalAccountant.CrystalSpent;
        int validLocalSaveCount = validLocalSave.Count;
        int localMaxPoints = MaxPoints;
        int localDonation = donationAccountant.Donation;
        int localClanNumber = (int)belongingToClan.SelectedClan;

        LoadData();

        List<int> validAccountSave = new List<int>();

        for (int i = 0; i < statisticsLength; i++)
        {
            if (statistics[i] != 1)
            {
                validAccountSave.Add(statistics[i]);
            }
        }

        int nonLocalStorageIndicesCount = statisticsLength - validLocalSaveCount;

        if (localMatchCount > accountMatchCount)
        {
            LoadMatchCount(localMatchCount);

            if (localMatchCount - accountMatchCount >= statisticsLength)
            {
                for (int i = 0; i < validLocalSaveCount; i++)
                {
                    statistics[i] = validLocalSave[i];
                }
            }
            else
            {
                if (nonLocalStorageIndicesCount <= validAccountSave.Count)
                {
                    for (int i = nonLocalStorageIndicesCount; i < validLocalSaveCount; i++)
                    {
                        statistics[i] = validLocalSave[i];
                    }
                }
                else
                {
                    for (int i = 0; i < statisticsLength; i++)
                    {
                        if (statistics[i] == 1)
                        {
                            int insertIndex = i;

                            for (int a = 0; a < validLocalSaveCount; a++)
                            {
                                statistics[insertIndex] = validLocalSave[a];

                                insertIndex++;
                            }

                            break;
                        }
                    }
                }
            }
        }

        if (localCrystalReceived + localCrystalSpent > accountSave.CrystalReceived + accountSave.CrystalSpent)
        {
            crystalAccountant.LoadCrystalCount(localCrystalReceived, localCrystalSpent);
        }

        if (localMaxPoints > MaxPoints)
        {
            MaxPoints = localMaxPoints;
        }

        if (localDonation > donationAccountant.Donation)
        {
            donationAccountant.LoadData(localDonation);
        }

        if ((int)belongingToClan.SelectedClan == 0 && localClanNumber != 0)
        {
            belongingToClan.LoadData(localClanNumber);
        }

        SetValuesToLeaderboards();

        SaveData();

        OnDataChecked?.Invoke();
    }

    private void SetValuesToLeaderboards()
    {
        YandexSDK.Instance.SetLeaderBoardScore("AfterMatchScore", currentResult);

        YandexSDK.Instance.SetLeaderBoardScore("MaxPoints", MaxPoints);

        if (belongingToClan.SelectedClan != Clans.ClansNames.None)
        {
            YandexSDK.Instance.SetLeaderBoardScore(LEADERBOARDSNAMES[(int)belongingToClan.SelectedClan - 1], donationAccountant.Donation);
        }
    }
}
